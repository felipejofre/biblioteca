/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Compra;
import modelo.CompraModelo;
import modelo.Distribuidor;
import modelo.DistribuidorModelo;
import modelo.Factura;
import modelo.FacturaModelo;
import modelo.Libro;
import modelo.LibroModelo;
import modelo.MetodoPago;
import modelo.MetodoPagoModelo;
import vista.AgregarLibroVista;
import vista.EditaEliminaCompraVista;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

/**
 * Esta clase implementa dos interfaces.
 * ActionListener: Para manipular los eventos que producen los botones en la vista.
 * MouseListener: Para manipular los eventos que produce el mouse en la vista.
 */
public class EditaYEliminaCompraControlador implements ActionListener{
    //Atributos de la clase.
    private EditaEliminaCompraVista vistaEditaEliminaCompra = new EditaEliminaCompraVista();
    private CompraModelo modeloCompra = new CompraModelo();
    private FacturaModelo modeloFactura = new FacturaModelo();
    private DistribuidorModelo modeloDistribuidor = new DistribuidorModelo();
    private MetodoPagoModelo modeloMetodoPago = new MetodoPagoModelo();
    private LibroModelo modeloLibro = new LibroModelo();
    private AgregarLibroVista vistaAgregarLibro = new AgregarLibroVista();
    //Variable que viene desde el mantenedor con la id de la compra que se quiere editar o eliminar.
    private int idCompraEditaElimina;
    /**
     * Constructor de la clase.
     * Recibe dos parámetros, la vista que maneja y la id del objeto que se quiere editar o eliminar.
     * @param vistaEditaEliminaCompra - Objeto del tipo EditaEliminaCompraVista.
     * @param idCompraEditaElimina - Objeto del tipo int con la id del objeto.
     */
    public EditaYEliminaCompraControlador(EditaEliminaCompraVista vistaEditaEliminaCompra,int idCompraEditaElimina){
        this.vistaEditaEliminaCompra = vistaEditaEliminaCompra;
        this.idCompraEditaElimina = idCompraEditaElimina;
        vistaEditaEliminaCompra.btnEditarCompra.addActionListener(this);
        vistaEditaEliminaCompra.btnEliminarCompra.addActionListener(this);
        vistaEditaEliminaCompra.btnCancelarEditarEliminar.addActionListener(this);
        
        cargarformularioVistaEditaElimina(idCompraEditaElimina);
        cargarCombos(idCompraEditaElimina);
        
        /**
         * Añadimos el objeto jText que recibe el precio neto a la interface addFocusListener
         * con la finalidad de saber cuando pierde el foco y realizar el cálculo.
         */
        vistaEditaEliminaCompra.txtPrecioNetoFactura.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            }
            
            @Override
            public void focusLost(FocusEvent e) {
                calcular();
            }
        });
        
    }
    /**
     * Sobreescribimos el metodo abstracto de la interface ActionListener.
     * @param e - Evento ActionEvent.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        //Preguntamos si el evento lo produce el botón editar.
        if (e.getSource() == vistaEditaEliminaCompra.btnEditarCompra) {
            editarCompra();
        }
        //Preguntamos si el evento lo produce el botón cancelar.
        if (e.getSource() == vistaEditaEliminaCompra.btnCancelarEditarEliminar) {
            vistaEditaEliminaCompra.dispose();
        }
        //Preguntamos si el evento lo produce el botón eliminar.
        if (e.getSource() == vistaEditaEliminaCompra.btnEliminarCompra) {
            eliminaCompra(idCompraEditaElimina);
        }
    }
    
    /**
     * Método que sigue el procedimiento para editar una compra.
     */
    private void editarCompra(){
        if (!validaFormulario()) {
            Compra compraEdita = modeloCompra.buscarCompraID(idCompraEditaElimina);
            Factura facturaEdita = modeloFactura.buscarFacturaID(compraEdita.getFactura().getId());
            
            //Entregamos los nuevos datos de la factura:
            facturaEdita.setFolio_fac(Integer.parseInt(vistaEditaEliminaCompra.txtFolioFactura.getText()));
            facturaEdita.setFecha_compra(vistaEditaEliminaCompra.jdFechaFactura.getDate());
            facturaEdita.setPrecio_neto(Double.parseDouble(vistaEditaEliminaCompra.txtPrecioNetoFactura.getText()));
            facturaEdita.setCosto_iva(Double.parseDouble(vistaEditaEliminaCompra.txtIvaFactura.getText()));
            facturaEdita.setPrecio_total(Double.parseDouble(vistaEditaEliminaCompra.txtTotalCompraFactura.getText()));
            
            //Asignando distribuidor:
            List<Distribuidor> distribuidores = modeloDistribuidor.listaDistribuidores();
            String distribuidorSeleccionado = String.valueOf(vistaEditaEliminaCompra.cmbDistribuidor.getSelectedItem());
            int idDistribuidor = -1;
            for (Distribuidor distribuidor : distribuidores) {
                if (distribuidor.getNombre().equalsIgnoreCase(distribuidorSeleccionado)) {
                    idDistribuidor = distribuidor.getId_dis();
                }
            }
            Distribuidor distribuidor = modeloDistribuidor.buscarDistribuidor(idDistribuidor);
            facturaEdita.setDistribuidor(distribuidor);
            
            //Asignamos metodo de pago:
            List<MetodoPago> metodosdepago = modeloMetodoPago.listaMetodoPagos();
            String metodoPagoSeleccionado = String.valueOf(vistaEditaEliminaCompra.cmbMetodoPago.getSelectedItem());
            int idMetodoPago = -1;
            for (MetodoPago metodoPago : metodosdepago) {
                if (metodoPago.getDescripcion().equalsIgnoreCase(metodoPagoSeleccionado)) {
                    idMetodoPago = metodoPago.getCod_metodo();
                }
            }
            MetodoPago metodopago = modeloMetodoPago.buscarMetodoPago(idMetodoPago);
            facturaEdita.setCod_metodo(metodopago);
            
            modeloFactura.editarFactura(facturaEdita);
            
            //Recolectamos libros seleccionados para la compra desde la tabla.
            //Para luego entregarle un set de libros a la compra.
            
            //Limpiamos la lista de libros que trae la compra que se quiere editar.
            if (!compraEdita.getLibrosSet().isEmpty()) {
                compraEdita.getLibrosSet().clear();
            }
            List<Libro> libros = modeloLibro.getListaLibros();
            
            if (vistaEditaEliminaCompra.tablaListaLibros.getSelectedRows().length > 0) {
                int[] filas = new int[vistaEditaEliminaCompra.tablaListaLibros.getSelectedRows().length];
                filas = vistaEditaEliminaCompra.tablaListaLibros.getSelectedRows();
                ArrayList<String> seriesLibros = new ArrayList<String>();
                
                for (int i = 0; i < vistaEditaEliminaCompra.tablaListaLibros.getSelectedRows().length; i++) {
                    String serielibro = String.valueOf(vistaEditaEliminaCompra.tablaListaLibros.getValueAt(filas[i], 0));
                    seriesLibros.add(serielibro);
                }
                
                Set<Libro> setLibrosnuevaCompra = new HashSet();
                
                for (String seriesLibro : seriesLibros) {
                    for (Libro libro : libros) {
                        if (libro.getSerie().equalsIgnoreCase(seriesLibro)) {
                            Libro libroS = modeloLibro.buscarLibroTabla(libro.getLibro_id());
                            setLibrosnuevaCompra.add(libroS);
                        }
                    }
                }
                //Recolectamos los datos de la compra.
                compraEdita.setPrecio(Double.parseDouble(vistaEditaEliminaCompra.txtTotalCompraFactura.getText()));
                compraEdita.setFactura(facturaEdita);
                compraEdita.setLibrosSet(setLibrosnuevaCompra);
                
                modeloCompra.editarCompra(compraEdita);
                
                vistaEditaEliminaCompra.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "La compra fue editada con éxito.");
                vistaEditaEliminaCompra.setAlwaysOnTop(true);
                
                System.out.println(compraEdita.toString());
                vistaEditaEliminaCompra.dispose();
            }else{
                vistaEditaEliminaCompra.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "No ha seleccionado libros de la tabla, la compra debe tener libros.");
                vistaEditaEliminaCompra.setAlwaysOnTop(true);
            }
        }else{
            vistaEditaEliminaCompra.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "Para editar una compra deben estar todos los campos completos.\nPor favor, complete todo el formulario.");
            vistaEditaEliminaCompra.setAlwaysOnTop(true);
        }
    }
    
    /**
     * Método que sigue el procedimiento para eliminar una compra.
     * @param idCompraEditaElimina - Int con la id de la compra que edita o elimina.
     */
    private void eliminaCompra(int idCompraEditaElimina){
        
        Compra compra = modeloCompra.buscarCompraID(idCompraEditaElimina);
        
        vistaEditaEliminaCompra.setAlwaysOnTop(false);
        int confirmado = JOptionPane.showConfirmDialog(null,"¿Quiere eliminar la compra con id: "+ compra.getId() +" de fecha "+ DateFormat.getDateInstance().format(compra.getFactura().getFecha_compra()) +"?");
        vistaEditaEliminaCompra.setAlwaysOnTop(true);
        
        if (JOptionPane.OK_OPTION == confirmado) {
            
            modeloCompra.eliminarCompra(idCompraEditaElimina);
            vistaEditaEliminaCompra.dispose();
            vistaEditaEliminaCompra.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null,"La compra fue eliminada definitivamente.");
            vistaEditaEliminaCompra.setAlwaysOnTop(true);
            
        }
        
        
    }
    
    /**
     * Método que carga el formulario con los datos de la compra.
     * @param idCompraEditaElimina - Int que tiene el id de la compra que edita o elimina.
     */
    private void cargarformularioVistaEditaElimina(int idCompraEditaElimina) {
        
        Compra compra = modeloCompra.buscarCompraID(idCompraEditaElimina);
        List<Libro> libros = modeloLibro.getListaLibros();
        DefaultTableModel modelo = (DefaultTableModel) vistaEditaEliminaCompra.tablaListaLibros.getModel();
        
        vistaEditaEliminaCompra.txtFolioFactura.setText(String.valueOf(compra.getFactura().getFolio_fac()));
        vistaEditaEliminaCompra.jdFechaFactura.setDate(compra.getFactura().getFecha_compra());
        
        vistaEditaEliminaCompra.txtPrecioNetoFactura.setText(String.valueOf(compra.getFactura().getPrecio_neto()));
        vistaEditaEliminaCompra.txtIvaFactura.setText(String.valueOf(compra.getFactura().getCosto_iva()));
        vistaEditaEliminaCompra.txtTotalCompraFactura.setText(String.valueOf(compra.getFactura().getPrecio_total()));
        
        
        for (Libro libro : compra.getLibrosSet()) {
            
            //Datos
            String serie = libro.getSerie();
            String isbn = libro.getIsbn();
            String titulo = libro.getTitulo();
            String pag = String.valueOf(libro.getPaginas());
            String precio = String.valueOf(libro.getPrecio());
            String anopub = String.valueOf(libro.getAno());
            String cantidad = String.valueOf(libro.getCantidad());
            String estado = String.valueOf(libro.getEstado().getDescripcion());
            String editorial = String.valueOf(libro.getEditorial().getNombre());
            
            String[] datos = {serie,isbn,titulo,pag,precio,anopub,cantidad,estado,editorial};
            
            modelo.addRow(datos);
            
        }
        vistaEditaEliminaCompra.tablaListaLibros.setRowSelectionInterval(0, modelo.getRowCount()-1);
        
        
    }
    /**
     * Método que valida el formulario.
     * Pregunta si el formulario está vacío.
     * @return validacion - boolean con la respuesta.
     */
    private boolean validaFormulario(){
        boolean folioVacio = vistaEditaEliminaCompra.txtFolioFactura.getText().isEmpty();
        boolean fechaVacia = vistaEditaEliminaCompra.jdFechaFactura.getDate() == null;
        boolean distribuidorVacio = vistaEditaEliminaCompra.cmbDistribuidor.getSelectedIndex() < 1;
        boolean metodopagoVacio = vistaEditaEliminaCompra.cmbMetodoPago.getSelectedIndex() < 1;
        boolean precionetoVacio = vistaEditaEliminaCompra.txtPrecioNetoFactura.getText().isEmpty();
        boolean noseleccionalibroVacio = vistaEditaEliminaCompra.tablaListaLibros.getSelectedRow() < 0;
        
        boolean validacion = folioVacio||fechaVacia||distribuidorVacio||metodopagoVacio||precionetoVacio||noseleccionalibroVacio;
        
        return validacion;
    }
    
    /**
     * Método que se encarga de traer los datos de la base de datos mediante
     * la ID de la compra.
     * @param idCompraEditaElimina - Int con la id de la compra a editar o eliminar. 
     */
    private void cargarCombos(int idCompraEditaElimina){
        Compra compra = modeloCompra.buscarCompraID(idCompraEditaElimina);
        String distribuidordeCompra = compra.getFactura().getDistribuidor().getNombre();
        String metodopagodeCompra = compra.getFactura().getCod_metodo().getDescripcion();
        
        
        int contador1 = 1;
        int seleccionar1 = 0;
        
        List<Distribuidor> distribuidores = modeloDistribuidor.listaDistribuidores();
        for (Distribuidor distribuidor : distribuidores) {
            vistaEditaEliminaCompra.cmbDistribuidor.addItem(distribuidor.getNombre());
            if (distribuidor.getNombre().equalsIgnoreCase(distribuidordeCompra)) {
                seleccionar1 = contador1;
            }
            contador1++;
        }
        vistaEditaEliminaCompra.cmbDistribuidor.setSelectedIndex(seleccionar1);
        
        int contador2 = 1;
        int seleccionar2 = 0;
        
        List<MetodoPago> metodosdepago = modeloMetodoPago.listaMetodoPagos();
        for (MetodoPago metodoPago : metodosdepago) {
            vistaEditaEliminaCompra.cmbMetodoPago.addItem(metodoPago.getDescripcion());
            if (metodoPago.getDescripcion().equalsIgnoreCase(metodopagodeCompra)) {
                seleccionar2 = contador2;
            }
            contador2++;
        }
        vistaEditaEliminaCompra.cmbMetodoPago.setSelectedIndex(seleccionar2);
    }
    
    /**
     * Método que calcula los valores del IVA y TOTAL cuando el campo
     * Total Neto pierde el foco.
     */
    private void calcular(){
        
        if (!vistaEditaEliminaCompra.txtPrecioNetoFactura.getText().isEmpty()) {
            try {
                int precioneto = Integer.parseInt(vistaEditaEliminaCompra.txtPrecioNetoFactura.getText());
                //calcular el precio del iva 19%.
                double precioIva = precioneto * 0.19;
                vistaEditaEliminaCompra.txtIvaFactura.setText(String.valueOf(precioIva));
                
                //Sumar precio total precionet + precioIva.
                double precioTotal = precioneto + precioIva;
                vistaEditaEliminaCompra.txtTotalCompraFactura.setText(String.valueOf(precioTotal));
            } catch (Exception e) {
                System.out.println("Error al calcular iva y total: "+e);
            }
            
        }
        
    }
    
}
