
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JOptionPane;
import modelo.*;
import vista.*;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

/**
 * La clase implementa la interface ActionListener para conocer los eventos
 * que se producen en la vista.
 */
public class AgregarLibroControlador implements ActionListener{
    
    //Atributos de la clase.
    private AgregarLibroVista vistaLibro = new AgregarLibroVista();
    private CategoriaModelo c = new CategoriaModelo();
    private AutorModelo a = new AutorModelo();
    private IdiomaModelo i = new IdiomaModelo();
    private EstadoModelo e = new EstadoModelo();
    private EditorialModelo ed = new EditorialModelo();
    private Libro nuevoLibro = new Libro();
    
    private List<Categoria> categorias = c.listaCategorias();
    public boolean noCategorias = categorias.isEmpty();
    
    private List<Autor> autores = a.listaAutores();
    public boolean noAutores = autores.isEmpty();
    
    private List<Idioma> idiomas = i.listaIdiomas();
    public boolean noIdiomas = idiomas.isEmpty();
    
    private List<Estado> estados = e.listaEstados();
    public boolean noEstados = estados.isEmpty();
    
    private List<Editorial> editoriales = ed.listaEditoriales();
    public boolean noEditoriales = editoriales.isEmpty();
    /**
     * Entrega un verdadero si falta algún atributo para crear un libro.
     * Es condición que deban existir todos los atributos del libro para ser creado.
     */
    private boolean faltanAtributosLibro = noCategorias||noAutores||noIdiomas||noEstados||noEditoriales;
    
    private LibroModelo l = new LibroModelo();
    MantenedorLibroVista manVista = new MantenedorLibroVista();
    
    /**
     * Constructor de la clase que recibe como parámetro una vista vistaLibro del tipo AgregarLibroVista
     * para manipularla y llevar los datos desde la vista al modelo.
     * @param vistaLibro - Objeto del tipo AgregarLibroVista.
     */
    public AgregarLibroControlador(AgregarLibroVista vistaLibro) {
        //Se comprueba si hay atributos necesarios para crear un objeto libro.
        faltanAtributos(vistaLibro);
        
        //Se inicializa el parámetro.
        this.vistaLibro = vistaLibro;
        //Se añade el botón guardar libro a la interfaz para manejar sus eventos.
        vistaLibro.btnGuardarLibro.addActionListener(this);
        //Se pide una serie a través del modelo y se le entrega al campo serie libro de la vista.
        vistaLibro.txtSerieLibro.setText(l.generarSerie());
        vistaLibro.btnCancelarAgregarLibro.addActionListener(this);
        
        cargarCombosLibro();
        
        //Añadirmos la vista a la interface WindowAdapter para saber cuando se cierra.
        vistaLibro.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e); //To change body of generated methods, choose Tools | Templates.
                //Cuando la ventana se cierra, limpiamos las listas de los jList para que no se dupliquen.
                vistaLibro.modeloCategorias.clear();
                vistaLibro.modeloAutores.clear();
                vistaLibro.modeloIdiomas.clear();
            }
            
        });
        
        
    }
    
    /**
     * Sobre escribimos el método actionPerformed de la interface ActionListener.
     * @param e - Evento ActionEvent.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        //Preguntamos si el evento lo produce el jButton btnCancelarAgregarLibro.
        if (e.getSource() == vistaLibro.btnCancelarAgregarLibro) {
            vistaLibro.dispose();
            vistaLibro.addWindowListener(new java.awt.event.WindowAdapter(){
                @Override
                public void windowClosed(WindowEvent e) {
                    super.windowClosed(e);
                    vistaLibro.modeloCategorias.clear();
                    vistaLibro.modeloAutores.clear();
                    vistaLibro.modeloIdiomas.clear();
                }
            });
            
        }
        //Preguntamos si el evento lo produce el jButton btnGuardarLibro.
        if (e.getSource() == vistaLibro.btnGuardarLibro) {
            //Preguntamos si todos los campos están con datos en la vista.
            if (validaFormulario()) {
                
                //Nos comunicamos con el modelo.
                LibroModelo addLibro = new LibroModelo();
                
                //Entrega atributos simples al objeto Libro.
                nuevoLibro.setTitulo(vistaLibro.txtTituloLibro.getText());
                nuevoLibro.setIsbn(vistaLibro.txtISBNLibro.getText());
                nuevoLibro.setSerie(vistaLibro.txtSerieLibro.getText());
                nuevoLibro.setPaginas(Integer.parseInt(vistaLibro.txtNumPaginas.getText()));
                nuevoLibro.setPrecio(Integer.parseInt(vistaLibro.txtPrecio.getText()));
                nuevoLibro.setAno(Integer.parseInt(vistaLibro.txtAnoPublicacion.getText()));
                nuevoLibro.setCantidad(Integer.parseInt(vistaLibro.txtCantidad.getText()));
                
                //Entrega codigo editorial al objeto libro.
                for (int j = 0; j < editoriales.size(); j++) {
                    if (j==vistaLibro.cmbEsitoriales.getSelectedIndex()-1) {
                        nuevoLibro.setEditorial(editoriales.get(j));
                    }
                }
                
                //Entrega codigo estado al objeto libro.
                for (int j = 0; j < estados.size(); j++) {
                    if (j == vistaLibro.cmbEstados.getSelectedIndex()-1) {
                        nuevoLibro.setEstado(estados.get(j));
                    }
                }
                
                //Atributos Listas del objeto Libro.
                //Set lista de categoria(as) seleccionadas.
                Set<Categoria> cateSet = new HashSet();
                for (int j = 0; j < vistaLibro.listCategorias.getSelectedValuesList().size(); j++) {
                    if (vistaLibro.listCategorias.getSelectedValuesList().get(j).equalsIgnoreCase(categorias.get(j).getNombre())) {
                        cateSet.add(new Categoria(categorias.get(j).getCod_cate(), categorias.get(j).getNombre()));
                    }
                }
                nuevoLibro.setCategoriasSet(cateSet);
                
                //Set lista de idioma(as) seleccionados.
                Set<Idioma> idioSet = new HashSet();
                for (int j = 0; j < vistaLibro.listIdiomas.getSelectedValuesList().size(); j++) {
                    if (vistaLibro.listIdiomas.getSelectedValuesList().get(j).equalsIgnoreCase(idiomas.get(j).getIdioma())) {
                        idioSet.add(new Idioma(idiomas.get(j).getCod_idioma(), idiomas.get(j).getIdioma()));
                    }
                }
                nuevoLibro.setIdiomasSet(idioSet);
                
                //Set lista de autor(es) seleccionados.
                Set<Autor> autSet = new HashSet();
                for (int j = 0; j < vistaLibro.listAutores.getSelectedValuesList().size(); j++) {
                    if (vistaLibro.listAutores.getSelectedValuesList().get(j).equalsIgnoreCase(autores.get(j).getNombre() + " " + autores.get(j).getApe_paterno() + " " + autores.get(j).getApe_materno())) {
                        autSet.add(new Autor(autores.get(j).getCod_autor(), autores.get(j).getNombre(), autores.get(j).getApe_paterno(), autores.get(j).getApe_materno()));
                    }
                }
                nuevoLibro.setAutorSet(autSet);
                //Se pide al modelo que registr el objeto libro en la base de datos.
                addLibro.crearLibro(nuevoLibro);
                //Se le comunica al usuario que el libro fue agregado con éxito a la base de datos.
                vistaLibro.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "<html><center>¡Libro "+ nuevoLibro.getTitulo() +" a sido guardado!.</center></html>");
                vistaLibro.setAlwaysOnTop(true);
                //Se procede a cerrar la ventana.
                vistaLibro.dispose();
                //Se añade la ventana a la interface WindowAdapter para saber cuando se cerró y
                //limpiar las listas de los objetos jList.
                vistaLibro.addWindowListener(new java.awt.event.WindowAdapter(){
                    @Override
                    public void windowClosed(WindowEvent e) {
                        super.windowClosed(e);
                        vistaLibro.modeloCategorias.clear();
                        vistaLibro.modeloAutores.clear();
                        vistaLibro.modeloIdiomas.clear();
                    }
                });
                
            }else{
                //Se le comunica al usuario que todos los campos deben estar completos para crear un libro.
                vistaLibro.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "<html><center>¡Campos Vacíos!<br>Debe completar todos los campos.</center></html>");
                vistaLibro.setAlwaysOnTop(true);
            }
        }
        
    }
    
    /**
     * Método que carga los selectores y listas de atributos registrados en la base de datos en la vista
     * que agrega un nuevo libro.
     */
    public void cargarCombosLibro(){
        
        for (Categoria categoria : categorias) {
            vistaLibro.modeloCategorias.addElement(categoria.getNombre());
        }
        
        for (Autor autor : autores) {
            vistaLibro.modeloAutores.addElement(autor.getNombre() + " " + autor.getApe_paterno() + " " + autor.getApe_materno());
        }
        
        for (Idioma idioma : idiomas) {
            vistaLibro.modeloIdiomas.addElement(idioma.getIdioma());
        }
        
        for (Estado estado : estados) {
            vistaLibro.cmbEstados.addItem(estado.getDescripcion());
        }
        
        for (Editorial editoriale : editoriales) {
            vistaLibro.cmbEsitoriales.addItem(editoriale.getNombre());
        }
        
    }
    
    /**
     * Método que valida el formulario.
     * Básicamente pregunta si hay campos vacíos.
     * Si hay campos vacios devuelve un true.
     * Si no hay campo vacíos devuelve un false.
     * @return bandera - boolean con respuesta a la validación.
     */
    public boolean validaFormulario(){
        boolean bandera = true;
        
        boolean tituloVacio = vistaLibro.txtTituloLibro.getText().isEmpty();
        boolean isbnVacio = vistaLibro.txtISBNLibro.getText().isEmpty();
        boolean serieVarcio = vistaLibro.txtSerieLibro.getText().isEmpty();
        boolean numPagVacio = vistaLibro.txtNumPaginas.getText().isEmpty();
        boolean precioVacio = vistaLibro.txtPrecio.getText().isEmpty();
        boolean anoPubVacio = vistaLibro.txtAnoPublicacion.getText().isEmpty();
        boolean cantidad = vistaLibro.txtCantidad.getText().isEmpty();
        
        if(tituloVacio||isbnVacio||serieVarcio||numPagVacio||precioVacio||anoPubVacio||cantidad) {
            bandera = false;
        }
        
        return bandera;
    }
    
    /**
     * Limpia los valores ingresados en el formulario.
     */
    public void limpiaFormularioLibro(){
        vistaLibro.txtTituloLibro.setText(null);
        vistaLibro.txtISBNLibro.setText(null);
        vistaLibro.txtSerieLibro.setText(l.generarSerie());
        vistaLibro.txtNumPaginas.setText(null);
        vistaLibro.txtPrecio.setText(null);
        vistaLibro.txtAnoPublicacion.setText(null);
        vistaLibro.txtCantidad.setText(null);
        vistaLibro.cmbEsitoriales.setSelectedIndex(0);
        vistaLibro.cmbEstados.setSelectedIndex(0);
        vistaLibro.listAutores.setSelectedValue(null, true);
        vistaLibro.listCategorias.setSelectedValue(null, true);
        vistaLibro.listIdiomas.setSelectedValue(null, true);
    }
    
    /**
     * Verifica que existan todos los atributos necesarios para crear un libro.
     * Le comunica al usuario cual es el error.
     * @param vistaAgregarLibro - Objeto del tipo AgregarLibroVista.
     */
    private void faltanAtributos(AgregarLibroVista vistaAgregarLibro){
        if (faltanAtributosLibro) {
            vistaAgregarLibro.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "Faltan atributos para crear un libro.");
            vistaAgregarLibro.setAlwaysOnTop(true);
            if (noCategorias) {
                vistaAgregarLibro.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "Para registrar un libro debe registrar antes al menos una Categoría.");
                vistaAgregarLibro.setAlwaysOnTop(true);
            }else if(noAutores){
                vistaAgregarLibro.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "Para registrar un libro debe registrar antes al menos un Autor.");
                vistaAgregarLibro.setAlwaysOnTop(true);
            }else if(noIdiomas){
                vistaAgregarLibro.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "Para registrar un libro debe registrar antes al menos un Idioma.");
                vistaAgregarLibro.setAlwaysOnTop(true);
            }else if(noEstados){
                vistaAgregarLibro.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "Para registrar un libro debe registrar antes al menos un Estado.");
                vistaAgregarLibro.setAlwaysOnTop(true);
            }else if(noEditoriales){
                vistaAgregarLibro.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "Para registrar un libro debe registrar antes al menos una Editorial.");
                vistaAgregarLibro.setAlwaysOnTop(true);
            }
            vistaAgregarLibro.dispose();
            
            vistaAgregarLibro.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    super.windowClosed(e); //To change body of generated methods, choose Tools | Templates.
                    vistaLibro.modeloCategorias.clear();
                    vistaLibro.modeloAutores.clear();
                    vistaLibro.modeloIdiomas.clear();
                    
                }
                
            });
            
        }
    }
    
    
    
    
}
