package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JOptionPane;
import modelo.Autor;
import modelo.AutorModelo;
import modelo.Categoria;
import modelo.CategoriaModelo;
import modelo.Editorial;
import modelo.EditorialModelo;
import modelo.Estado;
import modelo.EstadoModelo;
import modelo.Idioma;
import modelo.IdiomaModelo;
import modelo.Libro;
import modelo.LibroModelo;
import vista.EditaYEliminaLibroVista;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

/**
 * Esta clase es para editar y eliminar un libro seleccionado en la tabla con la lista
 * de libros registrados en la base de datos.
 */

//Esta clase implementa la clase "interface" que ejecuta evenlistener.
public class EditaYEliminaLibroControlador implements ActionListener{
    
    //Atributos del objeto Libro:
    //Atributo de la vista.
    private EditaYEliminaLibroVista vistaEditaLibro = new EditaYEliminaLibroVista();
    //Atributo objeto Categoria.
    private CategoriaModelo c = new CategoriaModelo();
    //Atributo objeto autor.
    private AutorModelo a = new AutorModelo();
    //Atributo objeto Idioma.
    private IdiomaModelo i = new IdiomaModelo();
    //Atributo objeto Estado.
    private EstadoModelo e = new EstadoModelo();
    //Atributo objeto Editorial.
    private EditorialModelo ed = new EditorialModelo();
    private int idLibro;
    //Atributo objeto Libro.
    
    //Listas de atributos que necesita la vista.
    List<Categoria> categorias = c.listaCategorias();
    List<Autor> autores = a.listaAutores();
    List<Idioma> idiomas = i.listaIdiomas();
    List<Estado> estados = e.listaEstados();
    List<Editorial> editoriales = ed.listaEditoriales();
    //Modelo para acceder a los datos.
    LibroModelo libroModelo;
    
    /**
     * Constructor de esta clase. Recibe dos parámetros que sirven para encontrar
     * y manipular el objeto que elimina o edita.
     * @param vistaEditaLibro - Vista que recibe los datos ingresados por el usurio.
     * @param idLibro - Int con la id del libro.
     */
    public EditaYEliminaLibroControlador(EditaYEliminaLibroVista vistaEditaLibro, int idLibro) {
        this.vistaEditaLibro = vistaEditaLibro;
        this.idLibro = idLibro;
        vistaEditaLibro.btnEditarLibro.addActionListener(this);
        vistaEditaLibro.btnEliminaLibro.addActionListener(this);
        vistaEditaLibro.bntCancelaEdicion.addActionListener(this);
        
        libroModelo = new LibroModelo();
        cargarCombosLibro(idLibro);
        
        //Añadirmos la ventana a una interface: Windows Listener para saber cuando se cierra.
        vistaEditaLibro.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                super.windowClosed(e);
                vistaEditaLibro.modeloCategorias.clear();
                vistaEditaLibro.modeloAutores.clear();
                vistaEditaLibro.modeloIdiomas.clear();
            }
            
        });
    }
    /**
     * Método abstracto sobre escrito para recibir los eventos que se producen en la vista.
     * @param e - Evento ActionEvent.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vistaEditaLibro.bntCancelaEdicion) {
            vistaEditaLibro.dispose();
        }
        
        if (e.getSource() == vistaEditaLibro.btnEliminaLibro) {
            vistaEditaLibro.setAlwaysOnTop(false);
            eliminarLibro();
            vistaEditaLibro.setAlwaysOnTop(true);
        }
        
        if (e.getSource() == vistaEditaLibro.btnEditarLibro) {
            editarLibro(idLibro);
        }
        
    }
    /**
     * Método que se encarga de cargar los combos y listas de atributos del libro en la ventana vista.
     * @param idLibro - Int con la id del libro que se quiere eliminar o editar.
     */
    public void cargarCombosLibro(int idLibro){
        LibroModelo l = new LibroModelo();
        Libro libroEditar = l.buscarLibroTabla(idLibro);
        
        int c = 0;
        ArrayList<Integer> indiceCategorias = new ArrayList<Integer>();
        for (Categoria categoria : categorias) {
            vistaEditaLibro.modeloCategorias.addElement(categoria.getNombre());
            for (Categoria categoriaLibro : libroEditar.getCategoriasSet()) {
                if (categoria.getNombre().equals(categoriaLibro.getNombre())) {
                    indiceCategorias.add(c);
                }
            }
            c++;
        }
        int[] indiceCategoriasSeleccion = new int[indiceCategorias.size()];
        for (int j = 0; j < indiceCategorias.size(); j++) {
            indiceCategoriasSeleccion[j] = indiceCategorias.get(j);
        }
        vistaEditaLibro.listCategorias.setSelectedIndices(indiceCategoriasSeleccion);
        
        int a = 0;
        ArrayList<Integer> indicesAutores = new ArrayList<Integer>();
        for (Autor autor : autores) {
            String autorGenera = autor.getNombre() + " " + autor.getApe_paterno() + " " + autor.getApe_materno();
            vistaEditaLibro.modeloAutores.addElement(autorGenera);
            for (Autor autorLibro : libroEditar.getAutorSet()) {
                String autorLibroEdita = autorLibro.getNombre()+" "+autorLibro.getApe_paterno()+" "+autorLibro.getApe_materno();
                if (autorGenera.equals(autorLibroEdita)) {
                    indicesAutores.add(a);
                }
            }
            a++;
        }
        int[] indicesAutoresSelecion = new int[indicesAutores.size()];
        for (int j = 0; j < indicesAutores.size(); j++) {
            indicesAutoresSelecion[j] = indicesAutores.get(j);
        }
        vistaEditaLibro.listAutores.setSelectedIndices(indicesAutoresSelecion);
        
        //Genero un acumulador de índices i.
        int i = 0;
        //ArrayList dinámico del tipo Integer para definir indiceIdiomas.
        ArrayList<Integer> indiceIdiomas = new ArrayList<Integer>();
        //Recorriendo la lista de idiomas completa de la base de datos.
        for (Idioma idioma : idiomas) {
            //Linea que añade todos los idiomas a la lista de idiomas en la vista.
            vistaEditaLibro.modeloIdiomas.addElement(idioma.getIdioma());
            //Ciclo para obtener los idiomas que corresponden al objeto editado.
            for (Idioma idiomaLibro : libroEditar.getIdiomasSet()) {
                if (idioma.getIdioma().equals(idiomaLibro.getIdioma())) {
                    //Si el idioma coincide, se le asigna al ArrayList indiceIdiomas.
                    indiceIdiomas.add(i);
                }
            }
            //Incremento en 1 cada vuelta el índice de mi variable i.
            i++;
        }
        
        //Traspasando el valor del array a un arreglo definido de enteros.
        int[] indiceIdiomasSeleccion = new int[indiceIdiomas.size()];
        //Vaciando los valores del array en el arreglo indiceIdiomasSeleccion.
        for (int j = 0; j < indiceIdiomas.size(); j++) {
            indiceIdiomasSeleccion[j] = indiceIdiomas.get(j);
        }
        //Agrego el arreglo a la lista en la vista con la selección de los idiomas.
        vistaEditaLibro.listIdiomas.setSelectedIndices(indiceIdiomasSeleccion);
        
        int defEstado = 1;
        int defineEstado = 0;
        for (Estado estado : estados) {
            vistaEditaLibro.cmbEstados.addItem(estado.getDescripcion());
            if (estado.getDescripcion().equals(libroEditar.getEstado().getDescripcion())) {
                defineEstado = defEstado;
            }
            defEstado++;
        }
        
        vistaEditaLibro.cmbEstados.setSelectedIndex(defineEstado);
        
        int defEditorial = 1;
        int defineEditorial = 0;
        for (Editorial editoriale : editoriales) {
            vistaEditaLibro.cmbEsitoriales.addItem(editoriale.getNombre());
            if (editoriale.getNombre().equals(libroEditar.getEditorial().getNombre())) {
                defineEditorial = defEditorial;
            }
            defEditorial++;
        }
        
        vistaEditaLibro.cmbEsitoriales.setSelectedIndex(defineEditorial);
        vistaEditaLibro.addWindowListener(new java.awt.event.WindowAdapter(){
            @Override
            public void windowClosed(WindowEvent e) {
                super.windowClosed(e);
                indiceCategorias.clear();
                indiceIdiomas.clear();
                indicesAutores.clear();
            }
        });
        
    }
    
    /**
     * Método que pregunta si el formulario tiene todos los campos completos.
     * @return bandera - boolean con la respuesta.
     */
    public boolean validaFormulario(){
        boolean bandera = true;
        
        boolean tituloVacio = vistaEditaLibro.txtTituloLibro.getText().isEmpty();
        boolean isbnVacio = vistaEditaLibro.txtISBNLibro.getText().isEmpty();
        boolean serieVarcio = vistaEditaLibro.txtSerieLibro.getText().isEmpty();
        boolean numPagVacio = vistaEditaLibro.txtNumPaginas.getText().isEmpty();
        boolean precioVacio = vistaEditaLibro.txtPrecio.getText().isEmpty();
        boolean anoPubVacio = vistaEditaLibro.txtAnoPublicacion.getText().isEmpty();
        boolean cantidadVacio = vistaEditaLibro.txtCantidad.getText().isEmpty();
        boolean estado = vistaEditaLibro.cmbEstados.getSelectedIndex() == 0;
        boolean editorial = vistaEditaLibro.cmbEsitoriales.getSelectedIndex() == 0;
        
        if(tituloVacio||isbnVacio||serieVarcio||numPagVacio||precioVacio||anoPubVacio||cantidadVacio||estado||editorial) {
            bandera = false;
        }
        
        return bandera;
    }
    /**
     * Método que sigue el proceso para editar el libro.
     * @param idLibro - Int con la id del libro que se quiere editar.
     */
    private void editarLibro(int idLibro) {
        if (validaFormulario()) {
            LibroModelo l = new LibroModelo();
            Libro libroEditar = l.buscarLibroTabla(idLibro);
            
            libroEditar.setTitulo(vistaEditaLibro.txtTituloLibro.getText());
            libroEditar.setIsbn(vistaEditaLibro.txtISBNLibro.getText());
            libroEditar.setPaginas(Integer.parseInt(vistaEditaLibro.txtNumPaginas.getText()));
            libroEditar.setPrecio(Double.parseDouble(vistaEditaLibro.txtPrecio.getText()));
            libroEditar.setAno(Integer.parseInt(vistaEditaLibro.txtAnoPublicacion.getText()));
            libroEditar.setCantidad(Integer.parseInt(vistaEditaLibro.txtCantidad.getText()));
            
            //Entrega codigo editorial al objeto libro.
            Editorial editorialNueva = new Editorial();
            for (Editorial editorial : editoriales) {
                if (editorial.getNombre().equals(vistaEditaLibro.cmbEsitoriales.getSelectedItem())) {
                    editorialNueva = editorial;
                }
            }
            libroEditar.setEditorial(editorialNueva);
            
            
            //Entrega codigo estado al objeto libro.
            Estado estadoNuevo = new Estado();
            for (Estado estado : estados) {
                if (estado.getDescripcion().equals(vistaEditaLibro.cmbEstados.getSelectedItem())) {
                    estadoNuevo = estado;
                }
            }
            libroEditar.setEstado(estadoNuevo);
            
            //Atributos Listas del objeto Libro.
            //Set lista de categoria(as) seleccionadas.
            //Creo el set de categorias nuevas.
            Set<Categoria> nuevoSetCategorias = new HashSet();
            //Recorro la lista de categorias de la base de datos.
            for (Categoria categoria : categorias) {
                //Recorro la lista de categorías seleccionadas en la vista del libro que edita.
                for (String seleccionCategorias : vistaEditaLibro.listCategorias.getSelectedValuesList()) {
                    //Pregunto si son iguales.
                    if (categoria.getNombre().equals(seleccionCategorias)) {
                        //Si coinciden entonces lo añado al set.
                        nuevoSetCategorias.add(categoria);
                    }
                }
            }
            //Le entrego el nuevo set al libro que estoy editando.
            libroEditar.setCategoriasSet(nuevoSetCategorias);
            
            //Set lista de idioma(as) seleccionados.
            Set<Idioma> nuevoIdiomaSet = new HashSet();
            for (Idioma idioma : idiomas) {
                for (String seleccionIcioma : vistaEditaLibro.listIdiomas.getSelectedValuesList()) {
                    if (idioma.getIdioma().equals(seleccionIcioma)) {
                        nuevoIdiomaSet.add(idioma);
                    }
                }
            }
            libroEditar.setIdiomasSet(nuevoIdiomaSet);
            
            //Set lista de autor(es) seleccionados.
            Set<Autor> nuevoAutoresSet = new HashSet();
            for (Autor autor : autores) {
                String autorape = autor.getNombre()+" "+autor.getApe_paterno()+" "+autor.getApe_materno();
                for (String seleccionAutores : vistaEditaLibro.listAutores.getSelectedValuesList()) {
                    if (autorape.equals(seleccionAutores)) {
                        nuevoAutoresSet.add(autor);
                    }
                }
            }
            libroEditar.setAutorSet(nuevoAutoresSet);
            
            l.editaLibro(libroEditar);
            
            vistaEditaLibro.dispose();
            
            JOptionPane.showMessageDialog(null, "<html><center>¡El libro Serie: "+ libroEditar.getSerie() +" a sido editado!.</center></html>");
        }else{
            JOptionPane.showMessageDialog(null, "<html><center>¡Campos Vacíos!<br>Debe completar todos los campos.</center></html>");
        }
    }
    /**
     * Método que sigue el proceso para eliminar el libro seleccionado.
     */
    private void eliminarLibro() {
        String serieElimina = vistaEditaLibro.txtSerieLibro.getText();
        int idLibroElimina = 0;
        LibroModelo l = new LibroModelo();
        
        List<Libro> libros = l.getListaLibros();
        
        for (Libro libro : libros) {
            if (serieElimina.equals(libro.getSerie())) {
                idLibroElimina = libro.getLibro_id();
            }
        }
        
        int confirmado = JOptionPane.showConfirmDialog(null,"¿Quieres borrar el libro con serie: "+ serieElimina +"?");
        
        if (JOptionPane.OK_OPTION == confirmado) {
            l.eliminaLibroID(idLibroElimina);
            LibroModelo libromodelo2 = new LibroModelo();
            vistaEditaLibro.dispose();
        }
    }
    
    
}
