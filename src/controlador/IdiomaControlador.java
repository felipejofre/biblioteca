/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Set;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Idioma;
import modelo.IdiomaModelo;
import modelo.Libro;
import modelo.LibroModelo;
import vista.IdiomaVista;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

/**
 * Clase que permite controlar las acciones que se producen en la vista Idiomas.
 */
public class IdiomaControlador implements ActionListener,MouseListener{
    //Atributos de la clase.
    private IdiomaVista vistaIdioma;
    private IdiomaModelo modeloIdioma = new IdiomaModelo();
    /**
     * Constructor de la clase que recibe como parámetro la vista idiomas.
     * @param vistaIdioma - Objeto del tipo IdiomaVista.
     */
    public IdiomaControlador(IdiomaVista vistaIdioma){
        this.vistaIdioma = vistaIdioma;
        vistaIdioma.btnGuardarIdioma.addActionListener(this);
        vistaIdioma.btnEditarIdioma.addActionListener(this);
        vistaIdioma.btnEliminarIdioma.addActionListener(this);
        vistaIdioma.tbListaIdiomas.addMouseListener(this);
        
        cargarTablaIdiomas();
    }
    /**
     * Método que pregunta si el formulario está completo.
     * @return bandera - boolean con la respuesta.
     */
    private boolean validaFormularioIdioma(){
        
        boolean nombre = vistaIdioma.txtNombreIdioma.getText().isEmpty();
        if (nombre) {
            vistaIdioma.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "Escriba un nombre para el idioma.");
            vistaIdioma.setAlwaysOnTop(true);
        }
        boolean muycorto = (vistaIdioma.txtNombreIdioma.getText().length()<4);
        if (muycorto) {
            vistaIdioma.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "Escriba un nombre más largo.");
            vistaIdioma.setAlwaysOnTop(true);
        }
        boolean bandera = nombre||muycorto;
        
        return bandera;
    }
    /**
     * Método que carga la tabla idiomas.
     */
    private void cargarTablaIdiomas(){
        List<Idioma> idiomas = modeloIdioma.listaIdiomas();
        DefaultTableModel modelo = (DefaultTableModel) vistaIdioma.tbListaIdiomas.getModel();
        
        int filas = modelo.getRowCount();
        
        try {
            for (int i = 0; i < filas; i++) modelo.removeRow(0);
        } catch (Exception e) {
            System.out.println(e);
        }
        
        for (Idioma idioma : idiomas) {
            String idiomaListar = idioma.getIdioma();
            String[] datos = {idiomaListar};
            modelo.addRow(datos);
        }
        
    }
    /**
     * Método que limpia el formulario.
     */
    private void limpiaFormulario(){
        vistaIdioma.txtNombreIdioma.setText(null);
    }
    
/**
 * Sobre escribimos el método abstracto qeu nos permite controlar los eventos producidos en la vista.
 * @param e - Evento ActionEvent.
 */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vistaIdioma.btnGuardarIdioma) {
            guardarIdioma();
        }
        
        if (e.getSource() == vistaIdioma.btnEliminarIdioma) {
            eliminarIdioma();
        }
        
        if (e.getSource() == vistaIdioma.btnEditarIdioma) {
            if (!validaFormularioIdioma()) {
                editarIdioma();
            }
        }
    }
/**
 * Sobre escribimos el método para saber donde hace clic el usuario en la tabla.
 * @param e - Evento MouseEvent.
 */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == vistaIdioma.tbListaIdiomas) {
            int firla = vistaIdioma.tbListaIdiomas.getSelectedRow();
            String Idioma = String.valueOf(vistaIdioma.tbListaIdiomas.getModel().getValueAt(firla, 0));
            
            vistaIdioma.txtNombreIdioma.setText(Idioma);
            
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
    /**
     * Método que sigue el proceso para guardar un Idioma.
     */
    private void guardarIdioma(){
        
        if (!validaFormularioIdioma()) {
            String idiomaAgrega = vistaIdioma.txtNombreIdioma.getText();
            boolean repetido = true;

            List<Idioma> idiomas = modeloIdioma.listaIdiomas();
            for (Idioma idioma : idiomas) {
                if (idioma.getIdioma().equalsIgnoreCase(idiomaAgrega)) {
                    repetido = false;
                }
            }

            if (repetido) {

                Idioma idiomaAgregar = new Idioma();
                idiomaAgregar.setIdioma(idiomaAgrega);
                modeloIdioma.crearIdioma(idiomaAgregar);
                vistaIdioma.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "El idioma fue agregado");
                vistaIdioma.setAlwaysOnTop(true);
                limpiaFormulario();
                cargarTablaIdiomas();

            }else{
                vistaIdioma.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "El idioma esta repetido. No fue agregado.");
                vistaIdioma.setAlwaysOnTop(true);
            }

        }
        
    }
    /**
     * Método que sirve para eliminar un idioma registrado en la base de datos.
     */
    private void eliminarIdioma(){
        
        int fila = vistaIdioma.tbListaIdiomas.getSelectedRow();
        if (fila>-1) {
            String nombre = String.valueOf(vistaIdioma.tbListaIdiomas.getModel().getValueAt(fila, 0));
            List<Idioma> idiomas = modeloIdioma.listaIdiomas();
            int idIdiomaElimina = -1;
            for (Idioma idioma : idiomas) {
                if (idioma.getIdioma().equalsIgnoreCase(nombre)) {
                    idIdiomaElimina = idioma.getCod_idioma();
                }
            }
            
            if (!(idIdiomaElimina == 8)) {
                
                Idioma idioma = modeloIdioma.buscarIdiomaID(idIdiomaElimina);
                
                if (!(idioma.getLibrosSet().isEmpty())) {
                    
                    vistaIdioma.setAlwaysOnTop(false);
                    int confirmado = JOptionPane.showConfirmDialog(null,"<html><center>Si borra este Idioma "+ idioma.getLibrosSet().size() +" Libros quedarán sin idioma.<br>"
                            + "¿Quiere borrar el idioma de todas maneras?</center></html>");
                    vistaIdioma.setAlwaysOnTop(true);
                    
                    if (JOptionPane.OK_OPTION == confirmado) {
                        LibroModelo modeloLibro = new LibroModelo();
                        Idioma idiomaEdita = modeloIdioma.buscarIdiomaID(idIdiomaElimina);
                        Idioma sinidioma = modeloIdioma.buscarIdiomaID(8);
                        
                        for (Libro libro : idiomaEdita.getLibrosSet()) {
                            Set<Idioma> idiomasLibro = libro.getIdiomasSet();
                            idiomasLibro.remove(idiomaEdita);
                            idiomasLibro.add(sinidioma);
                            libro.setIdiomasSet(idiomasLibro);
                            modeloLibro.editaLibro(libro);
                        }
                        
                        modeloIdioma.eliminaIdiomaID(idIdiomaElimina);
                        vistaIdioma.setAlwaysOnTop(false);
                        JOptionPane.showMessageDialog(null, "El Idioma " + nombre + " fue eliminado.");
                        vistaIdioma.setAlwaysOnTop(true);
                        limpiaFormulario();
                        cargarTablaIdiomas();
                    }
                    
                }else{
                    vistaIdioma.setAlwaysOnTop(false);
                    int confirmado = JOptionPane.showConfirmDialog(null,"¿Quieres borrar el idioma: "+ nombre +"?");
                    vistaIdioma.setAlwaysOnTop(true);
                    
                    if (JOptionPane.OK_OPTION == confirmado) {
                        modeloIdioma.eliminaIdiomaID(idIdiomaElimina);
                        vistaIdioma.setAlwaysOnTop(false);
                        JOptionPane.showMessageDialog(null, "El Idioma " + nombre + " fue eliminado.");
                        vistaIdioma.setAlwaysOnTop(true);
                        limpiaFormulario();
                        cargarTablaIdiomas();
                    }
                }
                
            }else{
                vistaIdioma.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "No puede ser eliminado. Es el idioma por defecto.");
                vistaIdioma.setAlwaysOnTop(true);
            }
            
            
        }else{
            vistaIdioma.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "Seleccione un idioma de la lista para eliminar.");
            vistaIdioma.setAlwaysOnTop(true);
        }
    }
    /**
     * Método que sigue el proceso para editar un idioma registrado en la base de datos.
     */
    private void editarIdioma(){
        int fila = vistaIdioma.tbListaIdiomas.getSelectedRow();
        
        if (fila>-1) {
            String nombre = String.valueOf(vistaIdioma.tbListaIdiomas.getModel().getValueAt(fila, 0));
            String nuevoNombre = vistaIdioma.txtNombreIdioma.getText();
            List<Idioma> idiomas = modeloIdioma.listaIdiomas();
            int idIdiomaEdita = -1;
            for (Idioma idioma : idiomas) {
                if (idioma.getIdioma().equalsIgnoreCase(nombre)) {
                    idIdiomaEdita = idioma.getCod_idioma();
                }
            }
            
            Idioma idiomaEditar = modeloIdioma.buscarIdiomaID(idIdiomaEdita);
            idiomaEditar.setIdioma(nuevoNombre);
            modeloIdioma.editaIdioma(idiomaEditar);
            
            vistaIdioma.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "El idioma "+ nombre +" fue editado.");
            vistaIdioma.setAlwaysOnTop(true);
            cargarTablaIdiomas();
            limpiaFormulario();
        }else{
            vistaIdioma.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "Seleccione un idioma de la lista para editar.");
            vistaIdioma.setAlwaysOnTop(true);
        }
        
    }
    
}