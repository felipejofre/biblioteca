/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Libro;
import modelo.LibroModelo;
import vista.AgregarLibroVista;
import vista.AutorVista;
import vista.CategoriaVista;
import vista.EditaYEliminaLibroVista;
import vista.EditorialVista;
import vista.EstadoVista;
import vista.IdiomaVista;
import vista.MantenedorLibroVista;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

/**
 * Clase que permite controlar las acciones que se producen en la vista Mantenedor de los libros.
 */
public class MantenedorLibroControlador implements ActionListener,MouseListener{
    //Atributos de la clase.
    private MantenedorLibroVista vistaMantenedorLibro = new MantenedorLibroVista();
    private LibroModelo libromodelo = new LibroModelo();
    /**
     * Constructor de la clase.
     * @param vistaMantenedorLibro - Objeto del tipo MantenedorLibroVista.
     */
    public MantenedorLibroControlador(MantenedorLibroVista vistaMantenedorLibro) {
        this.vistaMantenedorLibro = vistaMantenedorLibro;
        //Añadiendo los elementos al Listener para manipular sus eventos.
        vistaMantenedorLibro.btnAgregarLibro.addActionListener(this);
        vistaMantenedorLibro.btnEditaLibro.addActionListener(this);
        vistaMantenedorLibro.btnEliminarLibro.addActionListener(this);
        vistaMantenedorLibro.btnCategorias.addActionListener(this);
        vistaMantenedorLibro.btnAutores.addActionListener(this);
        vistaMantenedorLibro.btnIdiomas.addActionListener(this);
        vistaMantenedorLibro.btnEditoriales.addActionListener(this);
        vistaMantenedorLibro.btnEstados.addActionListener(this);
        
        vistaMantenedorLibro.tablaListaLibros.addMouseListener(this);
        
        cargarListaLibrosTabla(libromodelo.getListaLibros());
    }
    /**
     * Método abstracto para controlar los eventos que se producen en la vista.
     * @param e - Evento ActionEvent.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (e.getSource() == vistaMantenedorLibro.btnAgregarLibro) {
            if (AgregarLibroVista.ventanaVisible) {
                AgregarLibroVista vistaLibro = new AgregarLibroVista();
                
                vistaLibro.setVisible(true);
                vistaLibro.setLocationRelativeTo(null);
                
                vistaLibro.btnGuardarLibro.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        LibroModelo libromodelo2 = new LibroModelo();
                        cargarListaLibrosTabla(libromodelo2.getListaLibros());
                    }
                });
                
                AgregarLibroControlador controladorLibro = new AgregarLibroControlador(vistaLibro);
                
            }
        }
        
        if (e.getSource() == vistaMantenedorLibro.btnEditaLibro) {
            int fila = vistaMantenedorLibro.tablaListaLibros.getSelectedRow();
            if (fila>-1) {
                generaVentanaEditaElimina();
            }else{
                JOptionPane.showMessageDialog(null, "Elija un libro de la lista para editar.");
            }
            
        }
        
        if (e.getSource() == vistaMantenedorLibro.btnEliminarLibro) {
            int idLibro = 0;
            int fila = vistaMantenedorLibro.tablaListaLibros.getSelectedRow();
            if (fila>-1) {
                String serieLibro = String.valueOf(vistaMantenedorLibro.tablaListaLibros.getModel().getValueAt(fila, 0));
                
                LibroModelo modeloLibro = new LibroModelo();
                
                List<Libro> listaLibros = modeloLibro.getListaLibros();
                for (Libro listaLibro : listaLibros) {
                    if (listaLibro.getSerie().equals(serieLibro)) {
                        idLibro = listaLibro.getLibro_id();
                    }
                }
                int confirmado = JOptionPane.showConfirmDialog(null,"¿Quieres borrar el libro con serie: "+ serieLibro +"?");
                
                if (JOptionPane.OK_OPTION == confirmado) {
                    modeloLibro.eliminaLibroID(idLibro);
                    LibroModelo libromodelo2 = new LibroModelo();
                    cargarListaLibrosTabla(libromodelo2.getListaLibros());
                }
            }else{
                JOptionPane.showMessageDialog(null, "Elija un libro de la lista para eliminar.");
                
            }
        }
        
        if (e.getSource() == vistaMantenedorLibro.btnCategorias) {
            if (CategoriaVista.ventanaVisible) {
                CategoriaVista vistaCategoria = new CategoriaVista();
                vistaCategoria.setVisible(true);
                vistaCategoria.setLocationRelativeTo(null);
                vistaCategoria.setAlwaysOnTop(true);
                
                CategoriaControlador controlCategoria = new CategoriaControlador(vistaCategoria);
            }
            
            
        }
        if (e.getSource() == vistaMantenedorLibro.btnAutores) {
            if (AutorVista.ventanaVisible) {
                AutorVista vistaAutor = new AutorVista();
                vistaAutor.setVisible(true);
                vistaAutor.setLocationRelativeTo(null);
                vistaAutor.setAlwaysOnTop(true);
                
                AutorControlador controlAutor = new AutorControlador(vistaAutor);
            }
            
        }
        
        if (e.getSource() == vistaMantenedorLibro.btnIdiomas) {
            if (IdiomaVista.ventanaVisible) {
                IdiomaVista vistaIdioma = new IdiomaVista();
                vistaIdioma.setVisible(true);
                vistaIdioma.setLocationRelativeTo(null);
                vistaIdioma.setAlwaysOnTop(true);
                
                IdiomaControlador controlIdioma = new IdiomaControlador(vistaIdioma);
                
            }
        }
        if (e.getSource() == vistaMantenedorLibro.btnEditoriales) {
            if (EditorialVista.ventanActiva) {
                EditorialVista vistaEditorial = new EditorialVista();
                vistaEditorial.setVisible(true);
                vistaEditorial.setLocationRelativeTo(null);
                vistaEditorial.setAlwaysOnTop(true);
                
                EditorialControlador controlarEditorial = new EditorialControlador(vistaEditorial);
            }
        }
        
        if (e.getSource() == vistaMantenedorLibro.btnEstados) {
            if (EstadoVista.ventanaVisible) {
                EstadoVista vistaEstado = new EstadoVista();
                vistaEstado.setVisible(true);
                vistaEstado.setLocationRelativeTo(null);
                vistaEstado.setAlwaysOnTop(true);
                
                EstadoControlador controlarEstado = new EstadoControlador(vistaEstado);
            }
        }
        
    }
    
    /**
     * MÉTODO QUE CARGA LA TABLA DEL REGISTRO DE LIBROS DESDE LA BASE DE DATOS.
     * Recibe como parámetro una lista de libros.
     * @param libros List de objetos libros.
     */
    public void cargarListaLibrosTabla(List<Libro> libros){
        try {
            
            DefaultTableModel modelo = (DefaultTableModel) vistaMantenedorLibro.tablaListaLibros.getModel();
            
            int filas = modelo.getRowCount();
            
            try {
                for (int i = 0; i < filas; i++) modelo.removeRow(0);
            } catch (Exception e) {
                System.out.println(e);
            }
            
            for (Libro llb : libros) {
                //Datos
                String serie = llb.getSerie();
                String isbn = llb.getIsbn();
                String titulo = llb.getTitulo();
                String pag = String.valueOf(llb.getPaginas());
                String precio = String.valueOf(llb.getPrecio());
                String anopub = String.valueOf(llb.getAno());
                String cantidad = String.valueOf(llb.getCantidad());
                String estado = String.valueOf(llb.getEstado().getDescripcion());
                String editorial = String.valueOf(llb.getEditorial().getNombre());
                
                String[] datos = {serie,isbn,titulo,pag,precio,anopub,cantidad,estado,editorial};
                
                modelo.addRow(datos);
            }
        } catch (Exception e) {
            System.out.println("La lista sigue nula o vacía. "+e);
        }
        
    }
    /**
     * Genera una ventana para editar y eliminar un libro cuando se hace clic
     * en una fila de la tabla de lista con libros registrados en la base de datos.
     */
    public void generaVentanaEditaElimina(){
        if (EditaYEliminaLibroVista.ventanaVisible) {
            int fila = vistaMantenedorLibro.tablaListaLibros.getSelectedRow();
            int idLibro = 0;
            try {
                String serie = String.valueOf(vistaMantenedorLibro.tablaListaLibros.getModel().getValueAt(fila, 0));
                
                LibroModelo modeloLibro = new LibroModelo();
                
                List<Libro> listaLibros = modeloLibro.getListaLibros();
                for (Libro listaLibro : listaLibros) {
                    if (listaLibro.getSerie().equals(serie)) {
                        idLibro = listaLibro.getLibro_id();
                    }
                }
                
                Libro libro = modeloLibro.buscarLibroTabla(idLibro);
                
                EditaYEliminaLibroVista editaLibro = new EditaYEliminaLibroVista();
                editaLibro.setVisible(true);
                editaLibro.setLocationRelativeTo(null);
                
                editaLibro.btnEditarLibro.addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        LibroModelo libromodelo3 = new LibroModelo();
                        cargarListaLibrosTabla(libromodelo3.getListaLibros());
                        //editaLibro.dispose();
                    }
                });
                
                editaLibro.btnEliminaLibro.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        LibroModelo libroModelo4 = new LibroModelo();
                        cargarListaLibrosTabla(libroModelo4.getListaLibros());
                    }
                });
                
                EditaYEliminaLibroControlador controlEditaLibro = new EditaYEliminaLibroControlador(editaLibro, libro.getLibro_id());
                
                editaLibro.txtTituloLibro.setText(libro.getTitulo());
                editaLibro.txtISBNLibro.setText(libro.getIsbn());
                editaLibro.txtSerieLibro.setText(libro.getSerie());
                editaLibro.txtNumPaginas.setText(String.valueOf(libro.getPaginas()));
                editaLibro.txtPrecio.setText(String.valueOf(libro.getPrecio()));
                editaLibro.txtAnoPublicacion.setText(String.valueOf(libro.getAno()));
                editaLibro.txtCantidad.setText(String.valueOf(libro.getCantidad()));
                
                
            } catch (Exception ev) {
                System.out.println(ev);
            }
        }
    }
    
    /**
     * Método que carga el objeto libro en una nueva ventana
     * para editar o eliminar.
     * @param e guarda el evento cuando el usuario hace clic en una fila
     * de la tabla.
     **/
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == vistaMantenedorLibro.tablaListaLibros) {
            generaVentanaEditaElimina();
        }
        
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
    }
    
}
