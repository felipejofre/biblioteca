/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Editorial;
import modelo.EditorialModelo;
import modelo.Libro;
import modelo.LibroModelo;
import vista.EditorialVista;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

/**
 * Clase para controlar la vista de mantención para las editoriales.
 */
public class EditorialControlador implements ActionListener,MouseListener {
    //Atributos de la clase.
    EditorialVista vistaEditorial = new EditorialVista();
    EditorialModelo modeloEditorial = new EditorialModelo();
    /**
     * Constructor para esta clase.
     * @param vistaEditorial - Objeto del tipo EditorialVista.
     */
    public EditorialControlador(EditorialVista vistaEditorial) {
        this.vistaEditorial = vistaEditorial;
        vistaEditorial.btnGuardarEditorial.addActionListener(this);
        vistaEditorial.btnEditarEditorial.addActionListener(this);
        vistaEditorial.btnEliminarEditorial.addActionListener(this);
        vistaEditorial.tbListaEditoriales.addMouseListener(this);
        
        cargaTablaEditorial();
    }
    /**
     * Método abstracto sobreescrito para manipular los eventos que se producen en la vista.
     * @param e - Evento ActionEvent.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vistaEditorial.btnGuardarEditorial) {
            //Aquí guardo.
            agregarEditorial();
        }
        if (e.getSource() == vistaEditorial.btnEditarEditorial) {
            //Aquí edito.
            editarEditorial();
        }
        if (e.getSource() == vistaEditorial.btnEliminarEditorial) {
            //Aquí elimino.
            eliminaEditorial();
        }
    }
    /**
     * Método que sigue el proceso para registrar una editorial en la base de datos.
     */
    private void agregarEditorial(){
        if (!validaFormulario()) {
            Editorial nuevaEditorial = new Editorial();
            String nombreEditorialAgrega = vistaEditorial.txtNombreEditorial.getText();
            boolean bandera = true;
            
            List<Editorial> editoriales = modeloEditorial.listaEditoriales();
            
            for (Editorial editorial : editoriales) {
                if (editorial.getNombre().equalsIgnoreCase(nombreEditorialAgrega)) {
                    bandera = false;
                }
            }
            if (bandera) {
                nuevaEditorial.setNombre(nombreEditorialAgrega);
                modeloEditorial.crearEditorial(nuevaEditorial);
                JOptionPane.showMessageDialog(null, "Editorial guardada.");
                
            }else{
                vistaEditorial.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "<html><center>La editorial no se puede crear.<br>Está repetida.</center></html>");
                vistaEditorial.setAlwaysOnTop(true);
            }
            
            vistaEditorial.txtNombreEditorial.setText(null);
            
            cargaTablaEditorial();
            
            
        }else{
            //le digo que debe agregar un nombre.
            vistaEditorial.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "Por favor, ingrese un nombre para la editorial.");
            vistaEditorial.setAlwaysOnTop(true);
        }
    }
    /**
     * Método que sigue el proceso para editar una editorial registrada en la base de datos.
     */
    private void editarEditorial(){
        
        int fila = vistaEditorial.tbListaEditoriales.getSelectedRow();
        if (fila>-1) {
            String nombreAntiguo = String.valueOf(vistaEditorial.tbListaEditoriales.getModel().getValueAt(fila, 0));
            String nombreNuevo = vistaEditorial.txtNombreEditorial.getText();
            List<Editorial> editoriales = modeloEditorial.listaEditoriales();
            int idEdita = -1;
            
            for (Editorial editorial : editoriales) {
                if (editorial.getNombre().equalsIgnoreCase(nombreAntiguo)) {
                    idEdita = editorial.getCod_edi();
                }
            }
            
            
            vistaEditorial.setAlwaysOnTop(false);
            int confirmado = JOptionPane.showConfirmDialog(null,"<html><center>¿Quieres editar la editorial: "+ nombreAntiguo +"<br> a este nuevo nombre: "+ nombreNuevo +"?</center></html>");
            vistaEditorial.setAlwaysOnTop(true);
            
            if (JOptionPane.OK_OPTION == confirmado) {
                Editorial editorialEdita = modeloEditorial.buscarEditorialID(idEdita);
                editorialEdita.setNombre(nombreNuevo);
                modeloEditorial.editarEditorial(editorialEdita);
                vistaEditorial.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "Editorial editada.");
                vistaEditorial.setAlwaysOnTop(true);
                cargaTablaEditorial();
                vistaEditorial.txtNombreEditorial.setText(null);
            }
        }else{
            vistaEditorial.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "Por favor, seleccione una editorial de la lista.");
            vistaEditorial.setAlwaysOnTop(true);
        }
        
    }
    /**
     * Método que sigue el proceso para eliminar una editorial registrada en la base de datos.
     */
    private void eliminaEditorial(){
        
        int fila = vistaEditorial.tbListaEditoriales.getSelectedRow();
        
        if (fila>-1) {
            String nombreElimina = String.valueOf(vistaEditorial.tbListaEditoriales.getModel().getValueAt(fila, 0));
            List<Editorial> editoriales = modeloEditorial.listaEditoriales();
            int idElimina = -1;
            
            for (Editorial editorial : editoriales) {
                if (editorial.getNombre().equalsIgnoreCase(nombreElimina)) {
                    idElimina = editorial.getCod_edi();
                }
            }
            
            if (!(idElimina == 5)) {
                
                Editorial editorialQueElimina = modeloEditorial.buscarEditorialID(idElimina);
                
                if (editorialQueElimina.getLibros().isEmpty()) {
                    
                    vistaEditorial.setAlwaysOnTop(false);
                    int confirmado = JOptionPane.showConfirmDialog(null,"<html><center>¿Quieres eliminar la editorial: "+ nombreElimina +"?</center></html>");
                    vistaEditorial.setAlwaysOnTop(true);
                    
                    if (JOptionPane.OK_OPTION == confirmado) {
                        modeloEditorial.eliminarEditorialID(idElimina);
                        vistaEditorial.setAlwaysOnTop(false);
                        JOptionPane.showMessageDialog(null, "Editorial eliminada.");
                        vistaEditorial.setAlwaysOnTop(true);
                        cargaTablaEditorial();
                        vistaEditorial.txtNombreEditorial.setText(null);
                    }
                    
                }else{
                    
                    vistaEditorial.setAlwaysOnTop(false);
                    int confirmado = JOptionPane.showConfirmDialog(null,"<html><center>Si elimina esta Editorial "+editorialQueElimina.getLibros().size()+" libro(s) quedarán sin editorial.<br>"
                            + "¿Desea eliminar de todas formas?</center></html>");
                    vistaEditorial.setAlwaysOnTop(true);
                    
                    if (JOptionPane.OK_OPTION == confirmado) {
                        try {
                            Editorial editorialElimina = modeloEditorial.buscarEditorialID(idElimina);
                            Editorial editorialAgrega = modeloEditorial.buscarEditorialID(5);
                            List<Libro> librosEditar = editorialElimina.getLibros();
                            LibroModelo modeloLibro = new LibroModelo();
                            
                            for (Libro libro : librosEditar) {
                                libro.setEditorial(editorialAgrega);
                                modeloLibro.editaLibro(libro);
                            }
                            
                            modeloEditorial.eliminarEditorialID(idElimina);
                            
                            vistaEditorial.setAlwaysOnTop(false);
                            JOptionPane.showMessageDialog(null, "Editorial eliminada.");
                            vistaEditorial.setAlwaysOnTop(true);
                            cargaTablaEditorial();
                            vistaEditorial.txtNombreEditorial.setText(null);
                            
                        } catch (Exception ee) {
                            System.out.println(ee);
                        }
                        
                    }
                }
                
            }else{
                vistaEditorial.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "<html><center>Esta editorial no se puede borrar.<br>Porque es la editorial por defecto.</center></html>");
                vistaEditorial.setAlwaysOnTop(true);
            }
            
            
        }else{
            vistaEditorial.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "Por favor, seleccione una editorial de la lista para eliminar.");
            vistaEditorial.setAlwaysOnTop(true);
        }
        
        
        
    }
    /**
     * Método que se encarga de traer las editoriales desde la base de datos y cargarlas a la tabla o lista 
     * de editoriales en la vista.
     */
    private void cargaTablaEditorial(){
        DefaultTableModel modelo = (DefaultTableModel)vistaEditorial.tbListaEditoriales.getModel();
        List<Editorial> editoriales = modeloEditorial.listaEditoriales();
        
        int filas = modelo.getRowCount();
        
        try {
            for (int i = 0; i < filas; i++) modelo.removeRow(0);
        } catch (Exception e) {
            System.out.println(e);
        }
        
        for (Editorial editorial : editoriales) {
            
            String nombreEditorial = editorial.getNombre();
            
            String[] datos = {nombreEditorial};
            
            modelo.addRow(datos);
            
        }
        
        
    }
    /**
     * Método que pregunta si el formulario tiene todos los dtos requeridos.
     * @return bandera - boolean con la respuesta.
     */
    private boolean validaFormulario(){
        
        boolean nombreVacio = vistaEditorial.txtNombreEditorial.getText().isEmpty();
        boolean nombreCorto = (vistaEditorial.txtNombreEditorial.getText().length()<4);
        
        boolean bandera = nombreVacio||nombreCorto;
        
        return bandera;
    }
    
    /**
     * Se sobreescribe este método para obtener los datos desde donde se hace clic en la tabla.
     * @param e - Evento MouseEvent.
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == vistaEditorial.tbListaEditoriales) {
            int fila = vistaEditorial.tbListaEditoriales.getSelectedRow();
            
            String nombre = String.valueOf(vistaEditorial.tbListaEditoriales.getModel().getValueAt(fila, 0));
            
            vistaEditorial.txtNombreEditorial.setText(nombre);
        }
        
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
    }
    
}
