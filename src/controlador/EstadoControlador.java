package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Estado;
import modelo.EstadoModelo;
import modelo.Libro;
import modelo.LibroModelo;
import vista.EstadoVista;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

/**
 * Clase que permite controlar las acciones que se producen en la vista Estados.
 */
public class EstadoControlador implements ActionListener,MouseListener{
    //Atributos de la clase.
    EstadoVista vistaEstado = new EstadoVista();
    EstadoModelo modeloEstado = new EstadoModelo();
    /**
     * El constructor de la clase que recibe como parámetro la vista que va a controlar.
     * @param vistaEstado - Objeto del tipo EstadoVista.
     */
    public EstadoControlador(EstadoVista vistaEstado){
        this.vistaEstado = vistaEstado;
        vistaEstado.btnGuardarEstado.addActionListener(this);
        vistaEstado.btnEditarEstado.addActionListener(this);
        vistaEstado.btnEliminarEstado.addActionListener(this);
        vistaEstado.tbListaEstados.addMouseListener(this);
        
        cargarTablaEstados();
    }
    /**
     * Sobre escribimos el método abstracto para manejar los eventos producidos en la vista.
     * @param e - Evento ActionEvent.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vistaEstado.btnGuardarEstado) {
            //Aquí guardo
            crearEstado();
        }
        if (e.getSource() == vistaEstado.btnEditarEstado) {
            //Aquí edito
            editarEstado();
        }
        if (e.getSource() == vistaEstado.btnEliminarEstado) {
            //Aquí elimino.
            eliminarEstado();
        }
    }
    
    /**
     * Método que carga la tabla con los estados registrados en la base de datos.
     */
    private void cargarTablaEstados() {
        DefaultTableModel modelo = (DefaultTableModel)vistaEstado.tbListaEstados.getModel();
        List<Estado> estados = modeloEstado.listaEstados();
        
        int filas = modelo.getRowCount();
        
        try {
            for (int i = 0; i < filas; i++) modelo.removeRow(0);
        } catch (Exception e) {
            System.out.println(e);
        }
        
        for (Estado estado : estados) {
            String nombreEstado = estado.getDescripcion();
            
            String[] datos = {nombreEstado};
            
            modelo.addRow(datos);
        }
        
        
    }
    /**
     * Método que sigue el proceso para registrar un estado en la base de datos.
     */
    private void crearEstado(){
        if (!validaFormulario()) {
            String nombre = vistaEstado.txtNombreEstado.getText();
            List<Estado> estados = modeloEstado.listaEstados();
            boolean bandera = true;
            
            for (Estado estado : estados) {
                if (estado.getDescripcion().equalsIgnoreCase(nombre)) {
                    bandera = false;
                }
            }
            
            if (bandera) {
                Estado estado = new Estado();
                estado.setDescripcion(nombre);
                modeloEstado.agregarEstado(estado);
                
                vistaEstado.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "El nuevo estado fue agregado.");
                vistaEstado.setAlwaysOnTop(true);
                cargarTablaEstados();
                vistaEstado.txtNombreEstado.setText(null);
                
            }else{
                vistaEstado.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "No se creo. La editorial está repetida.");
                vistaEstado.setAlwaysOnTop(true);
            }
            
        }else{
            vistaEstado.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "Por favor, complete el nombre.");
            vistaEstado.setAlwaysOnTop(true);
        }
    }
    /**
     * Método que sigue el proceso para editar un estado registrado en la base de datos.
     */
    private void editarEstado(){
        int fila = vistaEstado.tbListaEstados.getSelectedRow();
        
        if (fila>-1) {
            if (!validaFormulario()) {
                String nombreAntiguo = String.valueOf(vistaEstado.tbListaEstados.getModel().getValueAt(fila, 0));
                String nombreNuevo = vistaEstado.txtNombreEstado.getText();
                List<Estado> estados = modeloEstado.listaEstados();
                int idEdita = -1;
                for (Estado estado : estados) {
                    if (estado.getDescripcion().equalsIgnoreCase(nombreAntiguo)) {
                        idEdita = estado.getCod_estado();
                    }
                }
                
                vistaEstado.setAlwaysOnTop(false);
                int confirmado = JOptionPane.showConfirmDialog(null,"<html><center>¿Quieres editar el estado: "+ nombreAntiguo +"<br> a este nuevo nombre: "+ nombreNuevo +"?</center></html>");
                vistaEstado.setAlwaysOnTop(true);
                
                if (JOptionPane.OK_OPTION == confirmado) {
                    Estado estado = new Estado();
                    estado.setCod_estado(idEdita);
                    estado.setDescripcion(nombreNuevo);
                    modeloEstado.editarEstado(estado);
                    vistaEstado.setAlwaysOnTop(false);
                    JOptionPane.showMessageDialog(null, "El estado fue editado.");
                    vistaEstado.setAlwaysOnTop(true);
                    cargarTablaEstados();
                    vistaEstado.txtNombreEstado.setText(null);
                }
                
                
            }
        }else{
            vistaEstado.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "Seleccione un estado de la lista para editar.");
            vistaEstado.setAlwaysOnTop(true);
        }
    }
    /**
     * Método que sigue el proceso para eliminar un estado registrado en la base de datos.
     */
    private void eliminarEstado(){
        int fila = vistaEstado.tbListaEstados.getSelectedRow();
        if (fila>-1) {
            if (!validaFormulario()) {
                
                String nombreElimina = String.valueOf(vistaEstado.tbListaEstados.getModel().getValueAt(fila, 0));
                List<Estado> estados = modeloEstado.listaEstados();
                int idElimina = -1;
                for (Estado estado : estados) {
                    if (estado.getDescripcion().equalsIgnoreCase(nombreElimina)) {
                        idElimina = estado.getCod_estado();
                    }
                }
                
                if (!(idElimina == 3)) {
                    
                    Estado estadoQueElimina = modeloEstado.buscarEstadoID(idElimina);
                    if (estadoQueElimina.getLibros().isEmpty()) {
                        vistaEstado.setAlwaysOnTop(false);
                        int confirmado = JOptionPane.showConfirmDialog(null,"<html><center>¿Quieres eliminar el estado: "+ nombreElimina +"?</center></html>");
                        vistaEstado.setAlwaysOnTop(true);
                        
                        if (JOptionPane.OK_OPTION == confirmado) {
                            modeloEstado.eliminarEstado(idElimina);
                            vistaEstado.setAlwaysOnTop(false);
                            JOptionPane.showMessageDialog(null, "El estado fue eliminado.");
                            vistaEstado.setAlwaysOnTop(true);
                            cargarTablaEstados();
                            vistaEstado.txtNombreEstado.setText(null);
                        }
                    }else{
                        
                        vistaEstado.setAlwaysOnTop(false);
                        int confirmado = JOptionPane.showConfirmDialog(null,"<html><center>Si elimina este estado. "+estadoQueElimina.getLibros().size()+" libros quedarán sin estado.<br>"
                                + "¿Desea eliminar de todos modos?</center></html>");
                        vistaEstado.setAlwaysOnTop(true);
                        
                        if (JOptionPane.OK_OPTION == confirmado) {
                            //Reasigno estados
                            LibroModelo modeloLibro = new LibroModelo();
                            Estado estadoNuevo = modeloEstado.buscarEstadoID(3);
                            for (Libro libro : estadoQueElimina.getLibros()) {
                                libro.setEstado(estadoNuevo);
                                modeloLibro.editaLibro(libro);
                            }
                            
                            modeloEstado.eliminarEstado(idElimina);
                            vistaEstado.setAlwaysOnTop(false);
                            JOptionPane.showMessageDialog(null, "El estado fue eliminado.");
                            vistaEstado.setAlwaysOnTop(true);
                            cargarTablaEstados();
                            vistaEstado.txtNombreEstado.setText(null);
                        }
                    }
                    
                }else{
                    vistaEstado.setAlwaysOnTop(false);
                    JOptionPane.showMessageDialog(null, "Este estado no se puede eliminar, porque es el estado por defecto.");
                    vistaEstado.setAlwaysOnTop(true);
                }
                
                
                
            }
        }else{
            vistaEstado.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "Seleccione un estado de la lista para eliminar.");
            vistaEstado.setAlwaysOnTop(true);
        }
    }
    /**
     * Método que pregunta si el formulario está completo.
     * @return bandera - boolean con la respuesta.
     */
    private boolean validaFormulario(){
        
        boolean nombreVacio = vistaEstado.txtNombreEstado.getText().isEmpty();
        boolean nombreCorto = (vistaEstado.txtNombreEstado.getText().length()<4);
        
        boolean bandera = nombreVacio||nombreCorto;
        
        return bandera;
    }
    /**
     * Método abstracto sobre escrito para saber en que parte de la tabla se hace click
     * @param e - Evento MouseEvent
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == vistaEstado.tbListaEstados) {
            int fila = vistaEstado.tbListaEstados.getSelectedRow();
            
            String nombre = String.valueOf(vistaEstado.tbListaEstados.getModel().getValueAt(fila, 0));
            
            vistaEstado.txtNombreEstado.setText(nombre);
        }
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
        
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
        
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
        
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
        
    }
    
    
}
