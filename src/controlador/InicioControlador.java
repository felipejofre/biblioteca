/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import vista.InicioVista;
import vista.MantenedorLibroVista;
import vista.AgregarLibroVista;
import vista.MantenedorComprasVista;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

/**
 * Clase que permite controlar las acciones que se producen en la vista Inicio.
 */
public class InicioControlador implements ActionListener{
    //Atributos de la clase.
    InicioVista vistaInicial = new InicioVista();
    /**
     * Cosntructor de la clase que recibe como parámetro la vista que controla.
     * @param vistaInicial - Objeto del tipo InicioVista.
     */
    public InicioControlador(InicioVista vistaInicial) {
        this.vistaInicial = vistaInicial;
        this.vistaInicial.btnLibrosAdmin.addActionListener(this);
        this.vistaInicial.btnAdminCompras.addActionListener(this);
        this.vistaInicial.menuAgregarLibro.addActionListener(this);
    }
    /**
     * Se sobre escribe el método para manejar los eventos producidos en la vista.
     * @param e - Evento ActionEvent.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vistaInicial.btnLibrosAdmin) {
            if (MantenedorLibroVista.ventanaActiva) {
                MantenedorLibroVista mantieneLibroVista = new MantenedorLibroVista();
                mantieneLibroVista.setVisible(true);
                mantieneLibroVista.setLocationRelativeTo(null);
                MantenedorLibroControlador mantenedorLibro = new MantenedorLibroControlador(mantieneLibroVista);
            }
        }
        
        if (e.getSource() == vistaInicial.btnAdminCompras) {
            if (MantenedorComprasVista.ventanaVisible) {
                MantenedorComprasVista vistaMantenedorCompras = new MantenedorComprasVista();
                vistaMantenedorCompras.setVisible(true);
                vistaMantenedorCompras.setLocationRelativeTo(null);
                vistaMantenedorCompras.setAlwaysOnTop(true);
                
                MantenedorComprasControlador controladorCompras = new MantenedorComprasControlador(vistaMantenedorCompras);
            }
        }
        
        
        if (e.getSource() == vistaInicial.menuAgregarLibro) {
            if (AgregarLibroVista.ventanaVisible) {
                
                if (MantenedorLibroVista.ventanaActiva) {
                    MantenedorLibroVista mantieneLibroVista = new MantenedorLibroVista();
                    mantieneLibroVista.setVisible(true);
                    mantieneLibroVista.setLocationRelativeTo(null);
                    MantenedorLibroControlador mantenedorLibro = new MantenedorLibroControlador(mantieneLibroVista);
                }
                
                AgregarLibroVista vistaAgregarLibro = new AgregarLibroVista();
                vistaAgregarLibro.setVisible(true);
                vistaAgregarLibro.setLocationRelativeTo(null);
                
                AgregarLibroControlador controladorAgregarLibro = new AgregarLibroControlador(vistaAgregarLibro);
                
            }
        }
        
        
    }
    
}
