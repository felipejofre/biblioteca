/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.MetodoPago;
import modelo.MetodoPagoModelo;
import vista.MediosDePagoVista;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

/**
 * Clase que permite controlar las acciones que se producen en la vista Método de pago..
 */
public class MetodoPagoControlador implements ActionListener,MouseListener{
    //Atributos de la clase.
    private MediosDePagoVista vistaMedioPago = new MediosDePagoVista();
    private MetodoPagoModelo modeloMetdoPago = new MetodoPagoModelo();
    /**
     * Constructor de la clase.
     * @param vistaMedioPago - Objeto del tipo MediosDePagoVista.
     */
    public MetodoPagoControlador(MediosDePagoVista vistaMedioPago){
        this.vistaMedioPago = vistaMedioPago;
        vistaMedioPago.setAlwaysOnTop(true);
        vistaMedioPago.btnGuardarMedioPago.addActionListener(this);
        vistaMedioPago.btnEditarMedioPago.addActionListener(this);
        vistaMedioPago.btnEliminarMedioPago.addActionListener(this);
        
        vistaMedioPago.tbListaMediosDePago.addMouseListener(this);
        cargaTablaListaMetodoPago();
    }
    /**
     * Método abstracto para manejar los eventos producidos en la vista.
     * @param e - Evento ActionEvent.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (e.getSource() == vistaMedioPago.btnGuardarMedioPago) {
            guardarMedioPago();
        }
        
        if (e.getSource() == vistaMedioPago.btnEditarMedioPago) {
            editaMedioPago();
        }
        
        if (e.getSource() == vistaMedioPago.btnEliminarMedioPago) {
            eliminaMedioPago();
        }
        
    }
    /**
     * Método abstracto para saber en que parte de la tabla se hace clic.
     * @param e - Evento MouseEvent.
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        int fila = vistaMedioPago.tbListaMediosDePago.getSelectedRow();
        String setNombre = String.valueOf(vistaMedioPago.tbListaMediosDePago.getValueAt(fila, 0));
        vistaMedioPago.txtNombreMediosDePago.setText(setNombre);
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
    }
    /**
     * Método que sigue el proceso para registrar un medio de pago en la base de datos.
     */
    private void guardarMedioPago() {
        
        
        if (!validaFormulario()) {
            
            List<MetodoPago> metodosdepago = modeloMetdoPago.listaMetodoPagos();
            String metodoAgrega = vistaMedioPago.txtNombreMediosDePago.getText();
            boolean estaRepetido = false;
            
            for (MetodoPago metodoPago : metodosdepago) {
                if (metodoPago.getDescripcion().equalsIgnoreCase(metodoAgrega)) {
                    estaRepetido = true;
                }
            }
            
            if (!estaRepetido) {
                
                MetodoPago metodoPagoAgrega = new MetodoPago();
                metodoPagoAgrega.setDescripcion(metodoAgrega);
                
                modeloMetdoPago.crearMetodoDePago(metodoPagoAgrega);
                
                vistaMedioPago.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "El medio de pago fue agregado.");
                vistaMedioPago.setAlwaysOnTop(true);
                
                cargaTablaListaMetodoPago();
                vistaMedioPago.txtNombreMediosDePago.setText(null);
                
            }else{
                vistaMedioPago.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "El medio de pago está repetido. No fue agregado.");
                vistaMedioPago.setAlwaysOnTop(true);
                vistaMedioPago.txtNombreMediosDePago.setText(null);
            }
            
            
        }else{
            vistaMedioPago.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "Por favor, ingrese un nombre para el medio de pago.");
            vistaMedioPago.setAlwaysOnTop(true);
        }
        
    }
    /**
     * Método que sigue el rpoceso para editar un medio de pago.
     */
    private void editaMedioPago(){
        int fila = vistaMedioPago.tbListaMediosDePago.getSelectedRow();
        if (fila>-1) {
            if (!validaFormulario()) {
                String nombreEditado = vistaMedioPago.txtNombreMediosDePago.getText();
                String nombre = String.valueOf(vistaMedioPago.tbListaMediosDePago.getValueAt(fila, 0));
                int idEdita = -1;
                
                List<MetodoPago> mediosdepago = modeloMetdoPago.listaMetodoPagos();
                
                for (MetodoPago metodoPago : mediosdepago) {
                    if (metodoPago.getDescripcion().equalsIgnoreCase(nombre)) {
                        idEdita = metodoPago.getCod_metodo();
                    }
                }
                
                MetodoPago metodoEditar = modeloMetdoPago.buscarMetodoPago(idEdita);
                metodoEditar.setDescripcion(nombreEditado);
                modeloMetdoPago.editaMeotodoPago(metodoEditar);
                
                vistaMedioPago.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "El metodo de pago fue editado.");
                vistaMedioPago.setAlwaysOnTop(true);
                cargaTablaListaMetodoPago();
                vistaMedioPago.txtNombreMediosDePago.setText(null);
            }
        }else{
            vistaMedioPago.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "Elija un Metodo de Pago de la lista para editar.");
            vistaMedioPago.setAlwaysOnTop(true);
        }
        
    }
    /**
     * Método que sigue el proceso para eliminar un medio de pago.
     */
    private void eliminaMedioPago(){
        int fila = vistaMedioPago.tbListaMediosDePago.getSelectedRow();
        if (fila>-1) {
            String MetodoPagoELimina = String.valueOf(vistaMedioPago.tbListaMediosDePago.getValueAt(fila, 0));
            List<MetodoPago> metodosdepago = modeloMetdoPago.listaMetodoPagos();
            int idElimina = -1;
            
            for (MetodoPago metodoPago : metodosdepago) {
                if (metodoPago.getDescripcion().equalsIgnoreCase(MetodoPagoELimina)) {
                    idElimina = metodoPago.getCod_metodo();
                }
            }
            
            MetodoPago metodo = modeloMetdoPago.buscarMetodoPago(idElimina);
            
            vistaMedioPago.setAlwaysOnTop(false);
            int confirmado = JOptionPane.showConfirmDialog(null,"¿Quiere eliminar el método de pago: "+metodo.getDescripcion()+"?");
            vistaMedioPago.setAlwaysOnTop(true);
            
            if (JOptionPane.OK_OPTION == confirmado) {
                
                modeloMetdoPago.eliminaMetodoPagoID(idElimina);
                
                vistaMedioPago.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "El método de pago fue eliminado.");
                vistaMedioPago.setAlwaysOnTop(true);
                
                cargaTablaListaMetodoPago();
                vistaMedioPago.txtNombreMediosDePago.setText(null);
                
            }
            
        }else{
            vistaMedioPago.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "Elija un Metodo de Pago de la lista para eliminar.");
            vistaMedioPago.setAlwaysOnTop(true);
        }
        
        
    }
    /**
     * Método que pregunta si el formulario está vacío o no.
     * @return validacion - boolean con la respuesta.
     */
    private boolean validaFormulario() {
        boolean validacion = true;
        
        boolean nombreVacio = vistaMedioPago.txtNombreMediosDePago.getText().isEmpty();
        
        validacion = nombreVacio;
        
        return validacion;
    }
    /**
     * Método que trae los métodos de pago registrados en la base de datos para
     * cargarlos en la tabla que lista los métodos de pago.
     */
    private void cargaTablaListaMetodoPago(){
        DefaultTableModel modelo = (DefaultTableModel) vistaMedioPago.tbListaMediosDePago.getModel();
        List<MetodoPago> metodosdePago = modeloMetdoPago.listaMetodoPagos();
        
        int filas = modelo.getRowCount();
        
        try {
            for (int i = 0; i < filas; i++) modelo.removeRow(0);
        } catch (Exception e) {
            System.out.println(e);
        }
        
        for (MetodoPago metodoPago : metodosdePago) {
            String nombreMetodo = metodoPago.getDescripcion();
            
            String[] datos = {nombreMetodo};
            
            modelo.addRow(datos);
            
        }
        
        
    }
    
}
