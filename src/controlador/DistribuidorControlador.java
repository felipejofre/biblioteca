package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Distribuidor;
import modelo.DistribuidorModelo;
import vista.DistribuidorVista;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

/**
 * Esta clase implementa dos interfaces.
 * ActionListener: Para manipular los eventos que producen los botones en la vista.
 * MouseListener: Para manipular los eventos que produce el mouse en la vista.
 */
public class DistribuidorControlador implements ActionListener,MouseListener{
    //Atributos de la clase.
    private DistribuidorVista vistaDistribuidor = new DistribuidorVista();
    private DistribuidorModelo modeloDistribuidor = new DistribuidorModelo();
    
    /**
     * El constructor de la clase que recibe como parámetro el la vista que va a manipular.
     * @param vistaDistribuidor - Objeto del tipo DistribuidorVista. 
     */
    public DistribuidorControlador(DistribuidorVista vistaDistribuidor){
        this.vistaDistribuidor = vistaDistribuidor;
        vistaDistribuidor.btnGuardarDistribuidor.addActionListener(this);
        vistaDistribuidor.btnEditaDistribuidor.addActionListener(this);
        vistaDistribuidor.btnEliminarDistribuidor.addActionListener(this);
        vistaDistribuidor.tbListaDistribuidores.addMouseListener(this);
        
        //Al iniciar la clase se cargan los distribuidores en la jTable.
        cargarTablaDistribuidores();
        
    }
    
    /**
     * Método abstracto de la interface ActionListener
     * @param e - Evento ActionEvent.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        //Consultamos si el evento lo produce el botón Guardar.
        if (e.getSource() == vistaDistribuidor.btnGuardarDistribuidor) {
            
            if (!validaFormulario()) {
                if (validaRut(vistaDistribuidor.txtRutDistribuidor.getText())) {
                    guardarDistribuidor();
                    cargarTablaDistribuidores();
                }
            }else{
                vistaDistribuidor.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "Hay campos vacíos.");
                vistaDistribuidor.setAlwaysOnTop(true);
                
            }
        }
        //Preguntamos si el evento lo produce el botón Editar.
        if (e.getSource() == vistaDistribuidor.btnEditaDistribuidor) {
            editarDistribuidor();
        }
        //Preguntamos si el evento lo produce el botón Eliminar.
        if (e.getSource() == vistaDistribuidor.btnEliminarDistribuidor) {
            eliminarDistribuidor();
        }
    }
    
    /**
     * Para capturar los datos seleecioandos por el usuario
     * en la lista de distribuidores, sobreescribimos el metodo abstracto de la
     * interface MouseListener.
     * @param e - Evento MouseEvent.
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == vistaDistribuidor.tbListaDistribuidores) {
            cargarDatosTablaAlFormulario();
        }
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
    }
    
    /**
     * Con este método se carga los datos capturados de la tabla al formulario.
     */
    private void cargarDatosTablaAlFormulario(){
        int fila = vistaDistribuidor.tbListaDistribuidores.getSelectedRow();
        String rut = String.valueOf(vistaDistribuidor.tbListaDistribuidores.getModel().getValueAt(fila, 0));
        String nombre = String.valueOf(vistaDistribuidor.tbListaDistribuidores.getModel().getValueAt(fila, 1));
        String ano = String.valueOf(vistaDistribuidor.tbListaDistribuidores.getModel().getValueAt(fila, 2));
        String fono = String.valueOf(vistaDistribuidor.tbListaDistribuidores.getModel().getValueAt(fila, 3));
        String pais = String.valueOf(vistaDistribuidor.tbListaDistribuidores.getModel().getValueAt(fila, 4));
        String comuna = String.valueOf(vistaDistribuidor.tbListaDistribuidores.getModel().getValueAt(fila, 5));
        String calle = String.valueOf(vistaDistribuidor.tbListaDistribuidores.getModel().getValueAt(fila, 6));
        String numero = String.valueOf(vistaDistribuidor.tbListaDistribuidores.getModel().getValueAt(fila, 7));
        
        vistaDistribuidor.txtRutDistribuidor.setText(rut);
        vistaDistribuidor.txtNombreDistribuidor.setText(nombre);
        vistaDistribuidor.txtAnoContratoDistribuidor.setText(ano);
        vistaDistribuidor.txtFonoDistribuidor.setText(fono);
        vistaDistribuidor.txtPaisDistribuidor.setText(pais);
        vistaDistribuidor.txtComunaDistribuidor.setText(comuna);
        vistaDistribuidor.txtCalleDistribuidor.setText(calle);
        vistaDistribuidor.txtNumeroCalle.setText(numero);
    }
    
    /**
     * Obtiene la lista de distribuidores y los carga en la jTable lista de distribuidores.
     */
    private void cargarTablaDistribuidores() {
        DefaultTableModel modelo = (DefaultTableModel)vistaDistribuidor.tbListaDistribuidores.getModel();
        List<Distribuidor> distribuidores = modeloDistribuidor.listaDistribuidores();
        
        int filas = modelo.getRowCount();
        
        try {
            for (int i = 0; i < filas; i++) modelo.removeRow(0);
        } catch (Exception e) {
            System.out.println(e);
        }
        
        for (Distribuidor distribuidor : distribuidores) {
            String rut = distribuidor.getRut_dis();
            String nombre = distribuidor.getNombre();
            String ano_contrato = String.valueOf(distribuidor.getAno_contrato());
            String fono = distribuidor.getFono();
            String pais = distribuidor.getPais();
            String comuna = distribuidor.getComuna();
            String calle = distribuidor.getCalle();
            String numeroCasa = String.valueOf(distribuidor.getNumero());
            
            
            String[] datos = {rut,nombre,ano_contrato,fono,pais,comuna,calle,numeroCasa};
            
            modelo.addRow(datos);
        }
        
    }
    
    /**
     * Metodo que sigue el procedimiento para almacenar un distribuidor en la base de datos.
     */
    private void guardarDistribuidor(){
        if (estaRepetido()) {
            
            if (!validaFormulario()) {
                
                Distribuidor distribuidor = new Distribuidor();
                distribuidor.setRut_dis(vistaDistribuidor.txtRutDistribuidor.getText());
                distribuidor.setNombre(vistaDistribuidor.txtNombreDistribuidor.getText());
                distribuidor.setAno_contrato(Integer.parseInt(vistaDistribuidor.txtAnoContratoDistribuidor.getText()));
                distribuidor.setFono(vistaDistribuidor.txtFonoDistribuidor.getText());
                distribuidor.setPais(vistaDistribuidor.txtPaisDistribuidor.getText());
                distribuidor.setComuna(vistaDistribuidor.txtComunaDistribuidor.getText());
                distribuidor.setCalle(vistaDistribuidor.txtCalleDistribuidor.getText());
                distribuidor.setNumero(Integer.parseInt(vistaDistribuidor.txtNumeroCalle.getText()));
                
                try {
                    modeloDistribuidor.crearDistribuidor(distribuidor);
                } catch (Exception e) {
                    vistaDistribuidor.setAlwaysOnTop(false);
                    JOptionPane.showMessageDialog(null, "Ocurrió un error al guardar el Distribuidor.");
                    vistaDistribuidor.setAlwaysOnTop(true);
                    System.out.println("Error: "+e);
                }
                
                
                vistaDistribuidor.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "El distribuidor fue guardado.");
                vistaDistribuidor.setAlwaysOnTop(true);
                
                limpiaFormulario();
                
                
            }else{
                vistaDistribuidor.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "Complete todos los campos.");
                vistaDistribuidor.setAlwaysOnTop(true);
                
            }
            
        }else{
            vistaDistribuidor.setAlwaysOnTop(false);
            
            JOptionPane.showMessageDialog(null, "El distribuidor está repetido.");
            vistaDistribuidor.setAlwaysOnTop(true);
            
        }
        
    }
    
    /**
     * Método que sigue el proceso para editar un distribuidor.
     */
    private void editarDistribuidor(){
        int fila = vistaDistribuidor.tbListaDistribuidores.getSelectedRow();
        if (fila>-1) {
            
            if (!validaFormulario()) {
                String rut = String.valueOf(vistaDistribuidor.tbListaDistribuidores.getValueAt(fila, 0));
                List<Distribuidor> distribuidores = modeloDistribuidor.listaDistribuidores();
                int idEdita = -1;
                
                for (Distribuidor distribuidor : distribuidores) {
                    if (distribuidor.getRut_dis().equalsIgnoreCase(rut)){
                        idEdita = distribuidor.getId_dis();
                    }
                }
                
                Distribuidor distribuidorEdita = modeloDistribuidor.buscarDistribuidor(idEdita);
                distribuidorEdita.setRut_dis(vistaDistribuidor.txtRutDistribuidor.getText());
                distribuidorEdita.setAno_contrato(Integer.parseInt(vistaDistribuidor.txtAnoContratoDistribuidor.getText()));
                distribuidorEdita.setCalle(vistaDistribuidor.txtCalleDistribuidor.getText());
                distribuidorEdita.setComuna(vistaDistribuidor.txtComunaDistribuidor.getText());
                distribuidorEdita.setFono(vistaDistribuidor.txtFonoDistribuidor.getText());
                distribuidorEdita.setNombre(vistaDistribuidor.txtNombreDistribuidor.getText());
                distribuidorEdita.setPais(vistaDistribuidor.txtPaisDistribuidor.getText());
                distribuidorEdita.setNumero(Integer.parseInt(vistaDistribuidor.txtNumeroCalle.getText()));
                
                modeloDistribuidor.editaDistribuidor(distribuidorEdita);
                
                vistaDistribuidor.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "¡El distribuidor: "+distribuidorEdita.getNombre()+" fue editado con éxito!");
                vistaDistribuidor.setAlwaysOnTop(true);
                cargarTablaDistribuidores();
                limpiaFormulario();
            }else{
                vistaDistribuidor.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "Todos los campos deben estar completos.");
                vistaDistribuidor.setAlwaysOnTop(true);
            }
            
            
        }else{
            
            vistaDistribuidor.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "Primero seleccione un proveedor de la lista para editar.");
            vistaDistribuidor.setAlwaysOnTop(true);
        }
        
        
    }
    
    /**
     * Método que sigue el procedimiento para eliminar un distribuidor.
     */
    private void eliminarDistribuidor(){
        
        int fila = vistaDistribuidor.tbListaDistribuidores.getSelectedRow();
        if (fila>-1) {
            String rutElimina = String.valueOf(vistaDistribuidor.tbListaDistribuidores.getModel().getValueAt(fila, 0));
            List<Distribuidor> distribuidores = modeloDistribuidor.listaDistribuidores();
            int idElimina = -1;
            
            for (Distribuidor distribuidor : distribuidores) {
                if (distribuidor.getRut_dis().equalsIgnoreCase(rutElimina)) {
                    idElimina = distribuidor.getId_dis();
                }
            }
            
            Distribuidor eliminarEsteDistribuidor = modeloDistribuidor.buscarDistribuidor(idElimina);
            
            vistaDistribuidor.setAlwaysOnTop(false);
            int confirmado = JOptionPane.showConfirmDialog(null,"¿Quiere eliminar el distribuidor: "+ eliminarEsteDistribuidor.getNombre() + "?");
            vistaDistribuidor.setAlwaysOnTop(true);
            
            
            if (JOptionPane.OK_OPTION == confirmado) {
                modeloDistribuidor.eliminaDistribuidorID(idElimina);
                
                vistaDistribuidor.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "¡El distribuidor "+ eliminarEsteDistribuidor.getNombre() + " fue eliminado!");
                vistaDistribuidor.setAlwaysOnTop(true);
                cargarTablaDistribuidores();
                limpiaFormulario();
            }
        }else{
            vistaDistribuidor.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "Debe seleccionar un distribuidor de la lista para eliminar.");
            vistaDistribuidor.setAlwaysOnTop(true);
        }
        
        
    }
    
    /**
     * Método que verifica que el distribuidor no esté ya registrado.
     * @return repetido - boolean con la respuesta.
     */
    private boolean estaRepetido(){
        
        boolean repetido = true;
        String rut = vistaDistribuidor.txtRutDistribuidor.getText();
        
        List<Distribuidor> distribuidores = modeloDistribuidor.listaDistribuidores();
        for (Distribuidor distribuidor : distribuidores) {
            if (distribuidor.getRut_dis().equalsIgnoreCase(rut)) {
                repetido = false;
            }
        }
        
        return repetido;
    }
    
    /**
     * Valida el formulario.
     * Pregunta si hay campos vacíos.
     * @return formularioValido - boolean con la respuesta.
     */
    private boolean validaFormulario(){
        
        boolean formularioValido = false;
        
        //Preguntamos si hay campso vacíos.
        boolean rutVacio = vistaDistribuidor.txtRutDistribuidor.getText().isEmpty();
        boolean nombreVacio = vistaDistribuidor.txtNombreDistribuidor.getText().isEmpty();
        boolean anoVacio = vistaDistribuidor.txtAnoContratoDistribuidor.getText().isEmpty();
        boolean fonoVacio = vistaDistribuidor.txtFonoDistribuidor.getText().isEmpty();
        boolean paisVacio = vistaDistribuidor.txtPaisDistribuidor.getText().isEmpty();
        boolean comunaVacio = vistaDistribuidor.txtComunaDistribuidor.getText().isEmpty();
        boolean calleVacia = vistaDistribuidor.txtCalleDistribuidor.getText().isEmpty();
        boolean numeroVacio = vistaDistribuidor.txtNumeroCalle.getText().isEmpty();
        
        formularioValido = rutVacio||nombreVacio||anoVacio||fonoVacio||paisVacio||comunaVacio||calleVacia||numeroVacio;
        
        
        return formularioValido;
    }
    
    /**
     * Algoritmo que verifica que el rut esté correcto.
     * @param Rut - String con la cadena de texto que se ingresa en el rut. Formato: 11111111-1
     * @return rutCorrecto - boolean con la respuesta.
     */
    private boolean validaRut(String Rut){
        
        boolean rutCorrecto;
        int multiplica = 0, contador = 2, acumula = 0, dV = 0,tam = Rut.length()-1;
        String dVerificador = null;
        
        for (int i = Rut.length()-3; i > -1; i--) {
            
            int digito = 0;
            
            try {
                digito = Integer.parseInt(String.valueOf(Rut.charAt(i)));
            } catch (Exception e) {
                System.out.println("El rut contiene letras." + e);
                rutCorrecto = false;
                break;
            }
            
            multiplica = digito * contador;
            
            acumula = acumula + multiplica;
            
            contador++;
            if (contador == 8) {
                contador = 2;
            }
        }
        dV = 11 - (acumula % 11);
        
        if (dV == 11) {
            dVerificador = "0";
        }else{
            if (dV == 10) {
                dVerificador = "k";
            }else{
                dVerificador = Integer.toString(dV);
            }
        }
        
        if (String.valueOf(Rut.charAt(tam)).equals(dVerificador)) {
            rutCorrecto = true;
        }else{
            rutCorrecto = false;
        }
        
        return rutCorrecto;
    }
    
    /**
     * Método que limpia el formulario.
     */
    private void limpiaFormulario(){
        vistaDistribuidor.txtAnoContratoDistribuidor.setText(null);
        vistaDistribuidor.txtCalleDistribuidor.setText(null);
        vistaDistribuidor.txtComunaDistribuidor.setText(null);
        vistaDistribuidor.txtFonoDistribuidor.setText(null);
        vistaDistribuidor.txtNombreDistribuidor.setText(null);
        vistaDistribuidor.txtNumeroCalle.setText(null);
        vistaDistribuidor.txtPaisDistribuidor.setText(null);
        vistaDistribuidor.txtRutDistribuidor.setText(null);
    }
    
}
