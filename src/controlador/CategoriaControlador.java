/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package controlador;

import modelo.*;
import vista.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Set;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

/**
 * Esta clase implementa dos interfaces.
 * ActionListener: Para manipular los eventos que producen los botones en la vista.
 * MouseListener: Para manipular los eventos que produce el mouse en la vista.
 */
public class CategoriaControlador implements ActionListener,MouseListener{
    //Atributos de la clase.
    CategoriaVista vistaCategoria = new CategoriaVista();
    CategoriaModelo modeloCategoria = new CategoriaModelo();
    
    /**
     * El constructor de esta clase recibe como parámetro la vista que controlará.
     * @param vistaCategoria - Objeto del tipo CategoriaVista.
     */
    public CategoriaControlador(CategoriaVista vistaCategoria) {
        this.vistaCategoria = vistaCategoria;
        this.vistaCategoria.btnAgregarCategoria.addActionListener(this);
        this.vistaCategoria.btnEditaCategoria.addActionListener(this);
        this.vistaCategoria.btnEliminarCategoria.addActionListener(this);
        
        this.vistaCategoria.tbListaCategoria.addMouseListener(this);
        
        //Cuando se construye esta clase de invoca el método que carga las categorías en el jTable.
        cargaTablaCategorias();
    }
    
    /**
     * Sobre escribiendo el método abstracto de la interface ActionListener.
     * @param e - Evento ActionEvent.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        
        //Preguntamos si el evento lo provoca el botón agregar categoría.
        if (e.getSource() == vistaCategoria.btnAgregarCategoria) {
            //Verificamos que el formulario no tenga errores o campos vacíos.
            if (condicion()) {
                //Si se produce eso se le informa el error al usuario.
                if (vistaCategoria.txtNombreCategoria.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Por favor, ingrese un nombre para la categoriía.");
                }else if (vistaCategoria.txtNombreCategoria.getText().length() < 4) {
                    JOptionPane.showMessageDialog(null, "Por favor, ingrese un nombre más largo.");
                }
                
            }else{
                //Si todo está correcto se procede a registrar la categoría en la base de datos.
                agregaCategoria();
            }
        }
        
        //Preguntamos si el evento lo produce el botón editar categoría.
        if (e.getSource() == vistaCategoria.btnEditaCategoria) {
            
            //Se consulta si el formulario no tiene errores.
            if (condicion()) {
                //Si el formulario tiene errores se le comunica al usuario.
                if (vistaCategoria.txtNombreCategoria.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Por favor, elija una categoría de la lista para editar.");
                }else if (vistaCategoria.txtNombreCategoria.getText().length() < 4) {
                    JOptionPane.showMessageDialog(null, "Por favor, ingrese un nombre más largo.");
                }
                
            }else{
                //Si el formulario no presenta errores se procede a la edición de la categoría.
                editaCategoria();
            }
        }
        
        //Si el botón corresponde a eliminar se procede a la eliminación previa confirmación del usuario.
        if (e.getSource() == vistaCategoria.btnEliminarCategoria) {
            //Se ejecuta el método que elimina la categoría.
            eliminarCategoria();
        }
        
    }
    
    /**
     * Método encargado de traer la lsita de categorías registradas en la base de datos
     * y cargarlas todas en la tabla lista jTable de las categorías.
     */
    private void cargaTablaCategorias() {
        DefaultTableModel modelo = (DefaultTableModel) vistaCategoria.tbListaCategoria.getModel();
        CategoriaModelo catModelo = new CategoriaModelo();
        List<Categoria> categorias = catModelo.listaCategorias();
        
        int filas = modelo.getRowCount();
        //Limpiamos la tabla de filas para que no se recargue la tabla con valores repetidos.
        try {
            for (int i = 0; i < filas; i++) modelo.removeRow(0);
        } catch (Exception e) {
            System.out.println(e);
        }
        //Cargamos la tabla con los nuevos valores actualizados.
        for (Categoria categoria : categorias) {
            String nombre_cat = categoria.getNombre();
            
            String[] datos = {nombre_cat};
            
            modelo.addRow(datos);
        }
        
    }
    
    /**
     * Método que procede en el registro de una categoría en la base de datos.
     * Captura los datos de la vista y los formatea, para entregarlos al modelo.
     */
    private void agregaCategoria() {
        String nombreC = vistaCategoria.txtNombreCategoria.getText();
        boolean repetido = false;
        
        CategoriaModelo cat = new CategoriaModelo();
        List<Categoria> categorias = cat.listaCategorias();
        
        for (Categoria categoria : categorias) {
            if (categoria.getNombre().equals(nombreC)) {
                repetido = true;
            }
        }
        
        if (repetido) {
            JOptionPane.showMessageDialog(null, "<html><center>¡No se puede crear la categoria!<br>"
                    + "La categoría está repetida.</center></html>");
        }else{
            Categoria c1 = new Categoria(nombreC);
            CategoriaModelo catAdd = new CategoriaModelo();
            catAdd.crearCategoria(c1);
            
            vistaCategoria.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "La categoria fue creada.");
            vistaCategoria.setAlwaysOnTop(true);
            
            vistaCategoria.txtNombreCategoria.setText(null);
            
            cargaTablaCategorias();
        }
        
    }
    
    /**
     * Método destinado a la eliminación de la categoría de la base de datos.
     */
    private void eliminarCategoria(){
        
        //Consultamos si el usuario seleccionó una categoría de la lista de categorías.
        int rowSelect = vistaCategoria.tbListaCategoria.getSelectedRow();
        
        //Si ha seleccionado una se procede.
        if (rowSelect>-1) {
            
            //Capturamos el nombre de la categoría que seleccionó.
            String nombre_categoria = vistaCategoria.txtNombreCategoria.getText();
            //Variable para capturar su ID.
            int idElimina = -1;
            
            //Creamos un objeto del modelo.
            CategoriaModelo cat = new CategoriaModelo();
            //Obtenemos la lista de categorías a través del modelo.
            List<Categoria> categorias = cat.listaCategorias();
            //Si la lista de categorías tiene no está vacía se procede.
            if (!categorias.isEmpty()) {
                
                //Recorremos la lista de categorías para buscar su ID.
                for (Categoria categoria : categorias) {
                    if (categoria.getNombre().equals(nombre_categoria)) {
                        idElimina = categoria.getCod_cate();
                    }
                }
                /**
                 * Si la categoría que quiere seleccionar corresponde a la id 38
                 * No se puede eliminar, es la categoría por defecto.
                 */
                if (!(idElimina == 38)) {
                    //Buscamos la categoría que quiere eliminar mediante el método del modelo.
                    Categoria categoriaElimina = cat.buscarCategoria(idElimina);
                    
                    /**
                     * Se procede a la reasignación de categorías a los libros que tienen dicha categoría.
                     * ya que MySQL no permitirá eliminar una categoría con indice relacionado a un registro en la
                     * tabla libro.
                     */
                    if (categoriaElimina.getLibrosSet().isEmpty()) {
                        Categoria catElimina = cat.buscarCategoria(idElimina);
                        vistaCategoria.setAlwaysOnTop(false);
                        int confirmado = JOptionPane.showConfirmDialog(null,"¿Quieres borrar la categoría: "+ catElimina.getNombre() +"?");
                        vistaCategoria.setAlwaysOnTop(true);
                        
                        if (JOptionPane.OK_OPTION == confirmado) {
                            cat.eliminaCategoriaID(idElimina);
                            cargaTablaCategorias();
                            vistaCategoria.txtNombreCategoria.setText(null);
                        }
                    }else{
                        vistaCategoria.setAlwaysOnTop(false);
                        int confirmado = JOptionPane.showConfirmDialog(null,"<html><center>Si borra esta categoría "+categoriaElimina.getLibrosSet().size()+" libro(s) quedarán sin categoría<br>"
                                + "¿Desea borrar de todas formas?</center></html>");
                        vistaCategoria.setAlwaysOnTop(true);
                        
                        if (JOptionPane.OK_OPTION == confirmado) {
                            Categoria categoriaQueElimina = modeloCategoria.buscarCategoria(idElimina);
                            Categoria categoriaQueAgrega = modeloCategoria.buscarCategoria(38);
                            LibroModelo modeloLibro = new LibroModelo();
                            Set<Libro> librosCategoriaElimina = categoriaQueElimina.getLibrosSet();
                            
                            for (Libro libro : librosCategoriaElimina) {
                                Set<Categoria> categoriasSetLibro = libro.getCategoriasSet();
                                categoriasSetLibro.remove(categoriaQueElimina);
                                categoriasSetLibro.add(categoriaQueAgrega);
                                libro.setCategoriasSet(categoriasSetLibro);
                                modeloLibro.editaLibro(libro);
                                
                            }
                            
                            cat.eliminaCategoriaID(idElimina);
                            cargaTablaCategorias();
                            vistaCategoria.txtNombreCategoria.setText(null);
                            vistaCategoria.setAlwaysOnTop(false);
                            JOptionPane.showMessageDialog(null, "La categoria fue eliminada.");
                            vistaCategoria.setAlwaysOnTop(true);
                        }
                    }
                }else{
                    //Si selecciona la categoría por defecto, se le informa al usuario que no puede ser eliminada.
                    vistaCategoria.setAlwaysOnTop(false);
                    JOptionPane.showMessageDialog(null, "No se puede eliminar, porque es la categoría por defecto.");
                    vistaCategoria.setAlwaysOnTop(true);
                }
                
                
                
                
            } else {
                //Cuando la lista de categorías está vacía se le comunica al usuario.
                vistaCategoria.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "¡No hay categorías creadas!");
                vistaCategoria.setAlwaysOnTop(true);
            }
        }else{
            //Si no ha seleccionado categoría de la lista, entonces se le comunica al usuario que debe seleccionar una.
            JOptionPane.showMessageDialog(null, "Seleccione una categoría de la lista para eliminar.");
        }
        
        
    }
    
    /**
     * Revisa que el formulario tenga los datos que correspondan.
     * @return condicion - boolean con la respuesta.
     */
    private boolean condicion() {
        boolean condicion = true;
        
        boolean nombreVacio = vistaCategoria.txtNombreCategoria.getText().isEmpty();
        boolean nombreCorto = (vistaCategoria.txtNombreCategoria.getText().length() < 4);
        
        condicion = nombreVacio || nombreCorto;
        
        return condicion;
    }
    
    /**
     * Método que crea el procedimiento para editar una categoría.
     */
    private void editaCategoria() {
        int fila = vistaCategoria.tbListaCategoria.getSelectedRow();
        String nombreAntiguo = String.valueOf(vistaCategoria.tbListaCategoria.getModel().getValueAt(fila, 0));
        
        int id = -1;
        String nombreEditado = vistaCategoria.txtNombreCategoria.getText();
        
        CategoriaModelo categoriaModelo = new CategoriaModelo();
        
        List<Categoria> categorias = categoriaModelo.listaCategorias();
        for (Categoria categoria : categorias) {
            if (categoria.getNombre().equals(nombreAntiguo)) {
                id = categoria.getCod_cate();
            }
        }
        
        Categoria catEdita = categoriaModelo.buscarCategoria(id);
        
        catEdita.setNombre(nombreEditado);
        
        categoriaModelo.editaCategoria(catEdita);
        
        vistaCategoria.setAlwaysOnTop(false);
        JOptionPane.showMessageDialog(null, "La categoria fue editada.");
        vistaCategoria.setAlwaysOnTop(true);
        
        cargaTablaCategorias();
        vistaCategoria.txtNombreCategoria.setText(null);
        
    }
    
    /**
     * Método abstracto de la interface MouseListener.
     * En este caso lo ocupo para capturar los valores de la fila seleccionada de la jTable.
     * @param e - Evento MouseEvent.
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == vistaCategoria.tbListaCategoria) {
            int fila = vistaCategoria.tbListaCategoria.getSelectedRow();
            
            String nomCate = String.valueOf(vistaCategoria.tbListaCategoria.getModel().getValueAt(fila, 0));
            
            vistaCategoria.txtNombreCategoria.setText(nomCate);
            
        }
        
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
    }
}
