
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Set;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Autor;
import modelo.AutorModelo;
import modelo.Libro;
import modelo.LibroModelo;
import vista.AutorVista;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

/**
 * La clase implementa las interfaces: ActionListener y MouseListener.
 * Con la finalidad de poder atender en este controlador los eventos de la vista.
 */
public class AutorControlador implements ActionListener,MouseListener{
    //Atributos de la clase.
    AutorVista vistaAutor;
    AutorModelo modeloAutor = new AutorModelo();
    
    /**
     * Constructor de la clase que recibe como parámetro un objeto del tipo AutorVista.
     * @param vistaAutor - Objeto del tipo AutorVista.
     */
    public AutorControlador(AutorVista vistaAutor) {
        this.vistaAutor = vistaAutor;
        vistaAutor.btnAgregarAutor.addActionListener(this);
        vistaAutor.btnEditarAutor.addActionListener(this);
        vistaAutor.btnEliminarAutor.addActionListener(this);
        vistaAutor.tbListaAutores.addMouseListener(this);
        cargaTablaAutores();
    }
    
    /**
     * Sobre escribimos el método abstracto de la interface ActionListener.
     * @param e - Evento ActionEvent.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        //Consultamos si el evento lo produce el botón agregar.
        if (e.getSource() == vistaAutor.btnAgregarAutor) {
            if (!validaFormularioAutor()) {
                guardaAutor();
            }else{
                vistaAutor.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "Debe completar todos los campos.");
                vistaAutor.setAlwaysOnTop(true);
            }
            
        }
        
        //Consultamos si el evento lo produce el botón editar.
        if (e.getSource() == vistaAutor.btnEditarAutor) {
            editarAutor();
        }
        
        //Consultamos si el evento lo produce el botón eliminar.
        if (e.getSource() == vistaAutor.btnEliminarAutor) {
            eliminarAutor();
        }
        
    }
    
    /**
     * Sobre escribimos el método abstracto de la interface MouseListener.
     * Esto con la finalidad de capturar los datos de la tabla lista autor.
     * @param e - Evento MouseEvent.
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == vistaAutor.tbListaAutores) {
            cargarDatosFormularioAutor();
        }
        
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
        
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
        
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
        
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
        
    }
    
    /**
     * Método para guardar el autor. Comunica los datos entregados por el usuario
     * en la vista y los entrega al modelo.
     */
    public void guardaAutor(){
        
        //Capturando los datos ingresados en la vista.
        String nombreAutor = vistaAutor.txtNombreAutor.getText();
        String ape_paternoAutor = vistaAutor.txtApellidoPaternoAutor.getText();
        String ape_maternoAutor = vistaAutor.txtApellidoMaternoAutor.getText();
        
        String autorAgrega = nombreAutor+" "+ape_paternoAutor+" "+ape_maternoAutor;
        boolean bandera = true;
        //Revisa si está repetido para permitir agregar o no.
        List<Autor> autores = modeloAutor.listaAutores();
        for (Autor autor : autores) {
            String autorDB = autor.getNombre()+" "+autor.getApe_paterno()+" "+autor.getApe_materno();
            if (autorAgrega.equalsIgnoreCase(autorDB)) {
                bandera = false;
            }
        }
        
        if (bandera) {
            //Se crea el objeto autor para ser registrado en la base de datos.
            Autor autorNuevo = new Autor();
            autorNuevo.setNombre(nombreAutor);
            autorNuevo.setApe_paterno(ape_paternoAutor);
            autorNuevo.setApe_materno(ape_maternoAutor);
            
            modeloAutor.crearAutor(autorNuevo);
            vistaAutor.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "El autor fue agregado");
            vistaAutor.setAlwaysOnTop(true);
            limpiarFormularioAutor();
            cargaTablaAutores();
        }else{
            //Si está repetido se le comunica al usuario.
            vistaAutor.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "<html><center>¡No se puede crear el autor!<br>El autor ya existe.</center></html>");
            vistaAutor.setAlwaysOnTop(true);
        }
        
        
        
    }
    
    /**
     * Función que valida el formulario.
     * Básicamente consulta que todos los campos del formularios tengan datos.
     * True si el formulario tiene campos vacíos.
     * False si todos los campos están con datos.
     * @return bandera - boolean con la respuesta.
     */
    public boolean validaFormularioAutor(){
        boolean nombreVacio = vistaAutor.txtNombreAutor.getText().isEmpty();
        boolean apellidoPaternoVacio = vistaAutor.txtApellidoPaternoAutor.getText().isEmpty();
        boolean apellidoMaternoVacio = vistaAutor.txtApellidoMaternoAutor.getText().isEmpty();
        
        boolean bandera = nombreVacio||apellidoPaternoVacio||apellidoMaternoVacio;
        
        return bandera;
    }
    
    /**
     * Método que limpia el formulario.
     */
    public void limpiarFormularioAutor(){
        vistaAutor.txtNombreAutor.setText(null);
        vistaAutor.txtApellidoPaternoAutor.setText(null);
        vistaAutor.txtApellidoMaternoAutor.setText(null);
    }
    
    /**
     * Seteamos los datos seleccionados por el usuario en el formulario con este método.
     */
    public void cargarDatosFormularioAutor(){
        int filaSelecionada = vistaAutor.tbListaAutores.getSelectedRow();
        String nombre = String.valueOf(vistaAutor.tbListaAutores.getModel().getValueAt(filaSelecionada, 0));
        vistaAutor.txtNombreAutor.setText(nombre);
        String ape_paterno = String.valueOf(vistaAutor.tbListaAutores.getModel().getValueAt(filaSelecionada, 1));
        vistaAutor.txtApellidoPaternoAutor.setText(ape_paterno);
        String ape_materno = String.valueOf(vistaAutor.tbListaAutores.getModel().getValueAt(filaSelecionada, 2));
        vistaAutor.txtApellidoMaternoAutor.setText(ape_materno);
    }
    
    /**
     * Método que carga la tabla de autores con los datos de la base de datos.
     */
    private void cargaTablaAutores() {
        DefaultTableModel modelo = (DefaultTableModel) vistaAutor.tbListaAutores.getModel();
        AutorModelo autorModelo = new AutorModelo();
        List<Autor> autores = autorModelo.listaAutores();
        
        int filas = modelo.getRowCount();
        
        try {
            for (int i = 0; i < filas; i++) modelo.removeRow(0);
        } catch (Exception e) {
            System.out.println(e);
        }
        
        for (Autor autor : autores) {
            String nombre = autor.getNombre();
            String ape_paterno = autor.getApe_paterno();
            String ape_materno = autor.getApe_materno();
            
            String[] datos = {nombre,ape_paterno,ape_materno};
            modelo.addRow(datos);
        }
    }
    
    /**
     * Método que hace el proceso de editar un usuario.
     */
    private void editarAutor() {
        int filaSeleccionada = vistaAutor.tbListaAutores.getSelectedRow();
        if (filaSeleccionada>-1) {
            //Atributos nuevo que el usuario edita.
            String nombre = vistaAutor.txtNombreAutor.getText();
            String apellidop = vistaAutor.txtApellidoPaternoAutor.getText();
            String apellidom = vistaAutor.txtApellidoMaternoAutor.getText();
            
            //Captura de los atributos antiguos para hacer la búsqueda.
            int fila = vistaAutor.tbListaAutores.getSelectedRow();
            String nombreAntiguo = String.valueOf(vistaAutor.tbListaAutores.getModel().getValueAt(fila, 0));
            String apePantiguo = String.valueOf(vistaAutor.tbListaAutores.getModel().getValueAt(fila, 1));
            String apeMantiguo = String.valueOf(vistaAutor.tbListaAutores.getModel().getValueAt(fila, 2));
            String autorFormularioAntiguo = nombreAntiguo+" "+apePantiguo+" "+apeMantiguo;
            
            int idEditaAutor = -1;
            
            List<Autor> autores = modeloAutor.listaAutores();
            for (Autor autor : autores) {
                if (autor.toString().equalsIgnoreCase(autorFormularioAntiguo)) {
                    idEditaAutor = autor.getCod_autor();
                }
            }
            
            Autor autorEditar = modeloAutor.buscarAutor(idEditaAutor);
            autorEditar.setNombre(nombre);
            autorEditar.setApe_paterno(apellidop);
            autorEditar.setApe_materno(apellidom);
            
            modeloAutor.editaAutor(autorEditar);
            
            vistaAutor.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "El autor fue editado.");
            vistaAutor.setAlwaysOnTop(true);
            cargaTablaAutores();
            limpiarFormularioAutor();
            
        }else{
            vistaAutor.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "Seleccione un autor desde la lista para editar.");
            vistaAutor.setAlwaysOnTop(true);
        }
    }
    
    /**
     * Método que sirve para eliminar un autor.
     * Comunicando la vista con el modelo y su método específico para esto.
     */
    private void eliminarAutor() {
        
        int filaSeleccionada = vistaAutor.tbListaAutores.getSelectedRow();
        if (filaSeleccionada>-1) {
            //Captura de los atributos antiguos para hacer la búsqueda.
            int fila = vistaAutor.tbListaAutores.getSelectedRow();
            String nombreAntiguo = String.valueOf(vistaAutor.tbListaAutores.getModel().getValueAt(fila, 0));
            String apePantiguo = String.valueOf(vistaAutor.tbListaAutores.getModel().getValueAt(fila, 1));
            String apeMantiguo = String.valueOf(vistaAutor.tbListaAutores.getModel().getValueAt(fila, 2));
            String autorElimina = nombreAntiguo+" "+apePantiguo+" "+apeMantiguo;
            
            int idAutorElimina = -1;
            
            List<Autor> autores = modeloAutor.listaAutores();
            for (Autor autor : autores) {
                if (autor.toString().equalsIgnoreCase(autorElimina)) {
                    idAutorElimina = autor.getCod_autor();
                }
            }
            
            if (!(idAutorElimina == 6)) {
                
                Autor autorqueElimina = modeloAutor.buscarAutor(idAutorElimina);
                if (autorqueElimina.getLibrosSet().isEmpty()) {
                    
                    vistaAutor.setAlwaysOnTop(false);
                    int confirmado = JOptionPane.showConfirmDialog(null,"¿Quieres borrar el autor: "+ autorElimina +"?");
                    vistaAutor.setAlwaysOnTop(true);
                    
                    if (JOptionPane.OK_OPTION == confirmado) {
                        modeloAutor.eliminaAutorID(idAutorElimina);
                        
                        vistaAutor.setAlwaysOnTop(false);
                        JOptionPane.showMessageDialog(null, "El autor "+ autorElimina +" fue eliminado.");
                        vistaAutor.setAlwaysOnTop(true);
                        cargaTablaAutores();
                        limpiarFormularioAutor();
                    }
                    
                }else{
                    vistaAutor.setAlwaysOnTop(false);
                    int confirmado = JOptionPane.showConfirmDialog(null,"<html><center>Al eliminar este autor "+autorqueElimina.getLibrosSet().size()+" Libros quedarán sin Autor"
                            + "¿Desea borrar de todas formas?</center></html>");
                    vistaAutor.setAlwaysOnTop(true);
                    
                    if (JOptionPane.OK_OPTION == confirmado) {
                        Autor autorQueElimina = modeloAutor.buscarAutor(idAutorElimina);
                        Autor autorQueAgrega = modeloAutor.buscarAutor(6);
                        Set<Libro> autoresLibro = autorQueElimina.getLibrosSet();
                        LibroModelo modeloLibro = new LibroModelo();
                        for (Libro libro : autoresLibro) {
                            Set<Autor> autoresDelLibro = libro.getAutorSet();
                            autoresDelLibro.remove(autorQueElimina);
                            autoresDelLibro.add(autorQueAgrega);
                            libro.setAutorSet(autoresDelLibro);
                            modeloLibro.editaLibro(libro);
                        }
                        
                        modeloAutor.eliminaAutorID(idAutorElimina);
                        vistaAutor.setAlwaysOnTop(false);
                        JOptionPane.showMessageDialog(null, "El autor "+ autorElimina +" fue eliminado.");
                        vistaAutor.setAlwaysOnTop(true);
                        cargaTablaAutores();
                        limpiarFormularioAutor();
                    }
                }
                
                
            }else{
                vistaAutor.setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "No se puede borrar. Es el autor por defecto.");
                vistaAutor.setAlwaysOnTop(true);
            }
        }else{
            vistaAutor.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "Seleccione un autor de la lista para eliminar");
            vistaAutor.setAlwaysOnTop(true);
        }
        
    }
    
}
