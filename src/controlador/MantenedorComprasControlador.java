/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import modelo.Compra;
import modelo.CompraModelo;
import vista.AgregarCompraVista;
import vista.DistribuidorVista;
import vista.EditaEliminaCompraVista;
import vista.MantenedorComprasVista;
import vista.MediosDePagoVista;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

/**
 * Clase que permite controlar las acciones que se producen en la vista Mantenedor de las compras.
 */
public class MantenedorComprasControlador implements ActionListener,MouseListener{
    //Atributos de la clase.
    MantenedorComprasVista vistaMantenedorCompra = new MantenedorComprasVista();
    CompraModelo modeloCompra = new CompraModelo();
    /**
     * Constructor de la clase.
     * @param vistaMantenedorCompra - Objeto del tipo  MantenedorComprasVista.
     */
    public MantenedorComprasControlador(MantenedorComprasVista vistaMantenedorCompra) {
        this.vistaMantenedorCompra = vistaMantenedorCompra;
        vistaMantenedorCompra.btnRegistrarCompra.addActionListener(this);
        vistaMantenedorCompra.btnDistribuidores.addActionListener(this);
        vistaMantenedorCompra.btnMetodosPago.addActionListener(this);
        vistaMantenedorCompra.btnEditarCompra.addActionListener(this);
        vistaMantenedorCompra.btnEliminarCompra.addActionListener(this);
        vistaMantenedorCompra.tbListaCompras.addMouseListener(this);
        
        
        cargarTablaCompras(modeloCompra.getListaCompras());
        
        
        
    }
    /**
     * Método abstracto sobre escrito para controlar los eventos producidos en la vista.
     * @param e - Evento ActionEvent.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vistaMantenedorCompra.btnRegistrarCompra) {
            if (AgregarCompraVista.vistaVisible) {
                AgregarCompraVista vistaAgregarCompra = new AgregarCompraVista();
                vistaAgregarCompra.setVisible(true);
                vistaAgregarCompra.setLocationRelativeTo(null);
                vistaAgregarCompra.setAlwaysOnTop(true);
                
                vistaMantenedorCompra.setAlwaysOnTop(false);
                
                vistaAgregarCompra.btnRegistrarCompra.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        CompraModelo modeloCompra1 = new CompraModelo();
                        cargarTablaCompras(modeloCompra1.getListaCompras());
                    }
                });
                
                
                
                vistaAgregarCompra.addWindowListener(new java.awt.event.WindowAdapter(){
                    @Override
                    public void windowClosed(WindowEvent e) {
                        super.windowClosed(e);
                        vistaMantenedorCompra.setAlwaysOnTop(true);
                        CompraModelo modeloCompra2 = new CompraModelo();
                        cargarTablaCompras(modeloCompra2.getListaCompras());
                    }
                });
                
                AgregarCompraControlador controlCompras = new AgregarCompraControlador(vistaAgregarCompra);
                
            }
        }
        
        if (e.getSource() == vistaMantenedorCompra.btnDistribuidores) {
            if (DistribuidorVista.ventanaVisible) {
                DistribuidorVista vistaDistribuidor = new DistribuidorVista();
                vistaDistribuidor.setVisible(true);
                vistaDistribuidor.setLocationRelativeTo(null);
                vistaDistribuidor.setAlwaysOnTop(true);
                vistaMantenedorCompra.setAlwaysOnTop(false);
                DistribuidorControlador controlDistribuidor = new DistribuidorControlador(vistaDistribuidor);
                
            }
        }
        
        if (e.getSource() == vistaMantenedorCompra.btnMetodosPago) {
            if (MediosDePagoVista.ventanaVisible) {
                MediosDePagoVista vistaMediosPago = new MediosDePagoVista();
                vistaMediosPago.setVisible(true);
                vistaMediosPago.setLocationRelativeTo(null);
                vistaMediosPago.setAlwaysOnTop(true);
                vistaMantenedorCompra.setAlwaysOnTop(false);
                
                MetodoPagoControlador metodopagoControl = new MetodoPagoControlador(vistaMediosPago);
            }
        }
        
        if (e.getSource() == vistaMantenedorCompra.btnEditarCompra) {
            int fila = vistaMantenedorCompra.tbListaCompras.getSelectedRow();
            if (EditaEliminaCompraVista.vistaVisible) {
                if (fila>-1) {
                    generaVistaEditaEliminaCompra();
                }else{
                    vistaMantenedorCompra.setAlwaysOnTop(false);
                    JOptionPane.showMessageDialog(null, "Seleccione una compra de la lista para editar.");
                    vistaMantenedorCompra.setAlwaysOnTop(true);
                }
            }
             
        }
        
        if (e.getSource() == vistaMantenedorCompra.btnEliminarCompra) {
            int fila = vistaMantenedorCompra.tbListaCompras.getSelectedRow();
            if (EditaEliminaCompraVista.vistaVisible) {
                if (fila>-1) {
                    generaVistaEditaEliminaCompra();
                }else{
                    vistaMantenedorCompra.setAlwaysOnTop(false);
                    JOptionPane.showMessageDialog(null, "Seleccione una compra de la lista para eliminar.");
                    vistaMantenedorCompra.setAlwaysOnTop(true);
                }
            }
        }
    }
    /**
     * Sobre escribir el método abstracto para identificar cuando se hace clic en la tabla.
     * @param e - Evento MouseEvent.
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == vistaMantenedorCompra.tbListaCompras) {
            generaVistaEditaEliminaCompra();
            vistaMantenedorCompra.setAlwaysOnTop(false);
        }
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
        
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
    }
    
    /**
     * método para entregar un formato a las columnas de la tabla.
     */
    private void tamanoColumnaTabla(){
        TableColumnModel modeloColumnas = vistaMantenedorCompra.tbListaCompras.getColumnModel();
        modeloColumnas.getColumn(0).setPreferredWidth(10);
        modeloColumnas.getColumn(0).setResizable(false);
        
    }
    /**
     * Método que carga la lista de compras en la tabla.
     * @param compras - Lista de objetos del tipo compra.
     */
    private void cargarTablaCompras(List<Compra> compras){
        tamanoColumnaTabla();
        DefaultTableModel modelo = (DefaultTableModel) vistaMantenedorCompra.tbListaCompras.getModel();
        
        int filas = modelo.getRowCount();
        
        try {
            for (int i = 0; i < filas; i++) modelo.removeRow(0);
        } catch (Exception e) {
            System.out.println(e);
        }
        
        SimpleDateFormat d = new SimpleDateFormat("dd-MM-yy");
        
        for (Compra compra : compras) {
            
            String idCompra = String.valueOf(compra.getId());
            String folioFac = String.valueOf(compra.getFactura().getFolio_fac());
            String precioNeto = String.valueOf(compra.getFactura().getPrecio_neto());
            String precioIva = String.valueOf(compra.getFactura().getCosto_iva());
            String precioTotal = String.valueOf(compra.getFactura().getPrecio_total());
            String fechaCompra = String.valueOf(DateFormat.getDateInstance().format(compra.getFactura().getFecha_compra()));
            String metodoPago = String.valueOf(compra.getFactura().getCod_metodo().getDescripcion());
            String distribuidorTipo = String.valueOf(compra.getFactura().getDistribuidor().getNombre());
            
            String[] datos = {idCompra,folioFac,precioNeto,precioIva,precioTotal,fechaCompra,metodoPago,distribuidorTipo};
            
            modelo.addRow(datos);
            
        }
        
        
        
    }
    /**
     * Método que genera una vista edita y eliminar una compra registrada en la base de datos.
     */
    private void generaVistaEditaEliminaCompra() {
        int fila = vistaMantenedorCompra.tbListaCompras.getSelectedRow();
        
        if (fila>-1) {
            int idCompraEditaElimina = Integer.parseInt(String.valueOf(vistaMantenedorCompra.tbListaCompras.getValueAt(fila, 0)));
            
            EditaEliminaCompraVista vistaEditaEliminaCompra = new EditaEliminaCompraVista();
            vistaEditaEliminaCompra.setVisible(true);
            vistaEditaEliminaCompra.setLocationRelativeTo(null);
            vistaEditaEliminaCompra.setAlwaysOnTop(true);
            vistaMantenedorCompra.setAlwaysOnTop(false);
            
            vistaEditaEliminaCompra.btnEditarCompra.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    CompraModelo modeloCompra3 = new CompraModelo();
                    cargarTablaCompras(modeloCompra3.getListaCompras());
                }
            });
            
            vistaEditaEliminaCompra.addWindowListener(new java.awt.event.WindowAdapter(){
                @Override
                public void windowClosed(WindowEvent e) {
                    super.windowClosed(e);
                    vistaMantenedorCompra.setAlwaysOnTop(true);
                    CompraModelo modeloCompra4 = new CompraModelo();
                    cargarTablaCompras(modeloCompra4.getListaCompras());
                }
            });
            
            EditaYEliminaCompraControlador controlEditaEliminaCompra = new EditaYEliminaCompraControlador(vistaEditaEliminaCompra, idCompraEditaElimina);
            
        }else{
            JOptionPane.showMessageDialog(null, "Seleccione una compra de la lista.");
        }
        
    }
    
}
