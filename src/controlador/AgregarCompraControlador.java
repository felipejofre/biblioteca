
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Compra;
import modelo.CompraModelo;
import modelo.Distribuidor;
import modelo.DistribuidorModelo;
import modelo.Factura;
import modelo.FacturaModelo;
import modelo.Libro;
import modelo.LibroModelo;
import modelo.MetodoPago;
import modelo.MetodoPagoModelo;
import vista.AgregarCompraVista;
import vista.AgregarLibroVista;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

/**
 * La clase implementa dos interfaces.
 * ActionListener: Para manipular los botones desde la vista.
 * MouseListener: Para saber los eventos que se generan mediante el periférico mouse.
 */
public class AgregarCompraControlador implements ActionListener,MouseListener{
    
    //Atributo del tipo vista que manipulará este controlador.
    AgregarCompraVista vistaAgregarCompra = new AgregarCompraVista();
    AgregarLibroVista vistaAgregarLibro = new AgregarLibroVista();
    
    //Los objetos que manejará este controlador. Atributos de la clase.
    LibroModelo modeloLibro = new LibroModelo();
    FacturaModelo modeloFactura = new FacturaModelo();
    CompraModelo modeloCompra = new CompraModelo();
    DistribuidorModelo modeloDistribuidor = new DistribuidorModelo();
    MetodoPagoModelo modeloMetodoPago = new MetodoPagoModelo();
    
    /**
     * Constructor de la clase controlador que recibe una vista como parámetro.
     * @param vistaAgregarCompra - Objeto del tipo AgregarCompraVista.
     */
    public AgregarCompraControlador(AgregarCompraVista vistaAgregarCompra) {
        //Asignación del parámetro.
        this.vistaAgregarCompra = vistaAgregarCompra;
        //Registramos el botón en la interface ActionListener para manejar sus eventos.
        vistaAgregarCompra.btnRegistrarCompra.addActionListener(this);
        //Le concedemos a la vista el atributo de que esté siempre visible.
        vistaAgregarCompra.setAlwaysOnTop(true);
        cargarListaLibrosTabla();
        cargarCombos();
        
        /**
         * Se añade el objeto jText txtPrecioNetoFactura a la interface FocusListener
         * para saber cuando pierde el foco y calcular los valores iva y total.
         */
        vistaAgregarCompra.txtPrecioNetoFactura.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            }
            
            @Override
            public void focusLost(FocusEvent e) {
                calcular();
            }
        });
        
        /**
         * Se agrega el objeto jButton btnGuardarLibro perteneciente al objeto vistaAgregarLibro
         * del tipo AgregarLibroVista a la interface ActionListener con la finalidad de
         * saber cuando se presiona y responder a ese evento desde esta clase reflejándolo en el objeto
         * vistaAgregarCompra del tipo AgregarCompraVista.
         */
        vistaAgregarLibro.btnGuardarLibro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == vistaAgregarLibro.btnGuardarLibro) {
                    
                    cargarListaLibrosTabla();
                    
                }
            }
        });
        
    }
    
    /**
     * Se sobree escribe método abstracto actionPerformed de la interface ActionListener.
     * @param e - Evento performed.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        //Pregunta si el evento es generado por el jButton btnRegistrarCompra.
        if (e.getSource() == vistaAgregarCompra.btnRegistrarCompra) {
            //Se lanza el método registrar compra.
            registrarCompra();
        }
        
    }
    
    
    @Override
    public void mouseClicked(MouseEvent e) {
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
    }
    
    /**
     * Metodo que se encarga de manipular lo que entrega la vista y conectarlo con el modelo.
     * Para crear un nuevo registro del tipo Compra en la base de datos.
     */
    private void registrarCompra(){
        
        //Preguntamos si se ingresaron todos los datos en el formualario.
        if (!validaFormulario()) {
            
            //Recolectamos datos de la factura
            int folio = Integer.parseInt(vistaAgregarCompra.txtFolioFactura.getText());
            double precioNeto = Double.parseDouble(vistaAgregarCompra.txtPrecioNetoFactura.getText());
            double costoIva = Double.parseDouble(vistaAgregarCompra.txtIvaFactura.getText());
            double precioTotal = Double.parseDouble(vistaAgregarCompra.txtTotalCompraFactura.getText());
            Date fecha = vistaAgregarCompra.jdFechaFactura.getDate();
            
            //Definiendo el metodo de pago seleccionado.
            List<MetodoPago> metodospagos = modeloMetodoPago.listaMetodoPagos();
            String metodoSeleccionado = String.valueOf(vistaAgregarCompra.cmbMetodoPago.getSelectedItem());
            int idMetodoPago = -1;
            for (MetodoPago metodopago : metodospagos) {
                if (metodopago.getDescripcion().equalsIgnoreCase(metodoSeleccionado)) {
                    idMetodoPago = metodopago.getCod_metodo();
                }
            }
            MetodoPago metodoSelect = modeloMetodoPago.buscarMetodoPago(idMetodoPago);
            
            //Definiendo el proveedor o distribuidor.
            List<Distribuidor> distribuidores = modeloDistribuidor.listaDistribuidores();
            String distribuidorSeleccionado = String.valueOf(vistaAgregarCompra.cmbDistribuidor.getSelectedItem());
            int idDistribuidor = -1;
            for (Distribuidor distribuidor : distribuidores) {
                if (distribuidor.getNombre().equalsIgnoreCase(distribuidorSeleccionado)) {
                    idDistribuidor = distribuidor.getId_dis();
                }
            }
            Distribuidor distribuidorSelect = modeloDistribuidor.buscarDistribuidor(idDistribuidor);
            
            //Objeto factura:
            Factura factura = new Factura();
            Factura facturaNueva = new Factura();
            facturaNueva.setFolio_fac(folio);
            facturaNueva.setPrecio_neto(precioNeto);
            facturaNueva.setCosto_iva(costoIva);
            facturaNueva.setPrecio_total(precioTotal);
            facturaNueva.setFecha_compra(fecha);
            facturaNueva.setCod_metodo(metodoSelect);
            facturaNueva.setDistribuidor(distribuidorSelect);
            
            //Intentamos registrar la factura creada.
            factura = modeloFactura.registrarFactura(facturaNueva);
            
            //Recolectamos libros seleccionados para la compra desde la tabla.
            //Para luego entregarle un set de libros a la compra.
            List<Libro> libros = modeloLibro.getListaLibros();
            
            int[] filas = new int[vistaAgregarCompra.tablaListaLibros.getSelectedRows().length];
            filas = vistaAgregarCompra.tablaListaLibros.getSelectedRows();
            ArrayList<String> seriesLibros = new ArrayList<String>();
            
            for (int i = 0; i < vistaAgregarCompra.tablaListaLibros.getSelectedRows().length; i++) {
                String serielibro = String.valueOf(vistaAgregarCompra.tablaListaLibros.getValueAt(filas[i], 0));
                seriesLibros.add(serielibro);
            }
            // se crea un set de libros que se adquirieron en esta compra.
            Set<Libro> setLibrosnuevaCompra = new HashSet();
            for (String seriesLibro : seriesLibros) {
                for (Libro libro : libros) {
                    if (libro.getSerie().equalsIgnoreCase(seriesLibro)) {
                        Libro libroS = modeloLibro.buscarLibroTabla(libro.getLibro_id());
                        setLibrosnuevaCompra.add(libroS);
                    }
                }
            }
            
            //Recolectamos los datos de la compra para enviarla al modelo.
            Compra compraNueva = new Compra();
            compraNueva.setPrecio(precioTotal);
            compraNueva.setFactura(factura);
            compraNueva.setLibrosSet(setLibrosnuevaCompra);
            //Enviamos la compra al modelo para que sea registrada.
            modeloCompra.registrarCompra(compraNueva);
            
            //Se le comunica al usuario qeu se registró la compra.
            vistaAgregarCompra.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "La compra fue registrada con éxito.");
            vistaAgregarCompra.setAlwaysOnTop(true);
            //Cerramos la ventana.
            vistaAgregarCompra.dispose();
        }else{
            //Se le comunica al usuario que debe ingresar todos los datos al formulario.
            vistaAgregarCompra.setAlwaysOnTop(false);
            JOptionPane.showMessageDialog(null, "Por favor, complete todos los campos.");
            vistaAgregarCompra.setAlwaysOnTop(true);
        }
        
    }
    
    /**
     * Método que calcula el iva y el total una vez se ingresa el valor neto de la factura.
     * Se considera un 19% de IVA.
     */
    private void calcular(){
        
        if (!vistaAgregarCompra.txtPrecioNetoFactura.getText().isEmpty()) {
            try {
                int precioneto = Integer.parseInt(vistaAgregarCompra.txtPrecioNetoFactura.getText());
                //calcular el precio del iva 19%.
                double precioIva = precioneto * 0.19;
                vistaAgregarCompra.txtIvaFactura.setText(String.valueOf(precioIva));
                
                //Sumar precio total precionet + precioIva.
                double precioTotal = precioneto + precioIva;
                vistaAgregarCompra.txtTotalCompraFactura.setText(String.valueOf(precioTotal));
            } catch (Exception e) {
                System.out.println("Error al calcular iva y total: "+e);
            }
            
        }
        
    }
    
    /**
     * Método que se encarga de cargar los combos de la vista que sirve para
     * registrar una nueva compra en la base de datos.
     */
    private void cargarCombos(){
        List<Distribuidor> distribuidores = modeloDistribuidor.listaDistribuidores();
        for (Distribuidor distribuidor : distribuidores) {
            vistaAgregarCompra.cmbDistribuidor.addItem(distribuidor.getNombre());
        }
        
        List<MetodoPago> metodosdepago = modeloMetodoPago.listaMetodoPagos();
        for (MetodoPago metodoPago : metodosdepago) {
            vistaAgregarCompra.cmbMetodoPago.addItem(metodoPago.getDescripcion());
        }
    }
    
    /**
     * Método que agrega la lista de libros a la tabla de la vista que registra
     * una compra en la base de datos para que sean seleccionados por el usuario.
     */
    private void cargarListaLibrosTabla(){
        try {
            DefaultTableModel modelo = (DefaultTableModel) vistaAgregarCompra.tablaListaLibros.getModel();
            List<Libro> libros = modeloLibro.getListaLibros();
            int filas = modelo.getRowCount();
            
            try {
                for (int i = 0; i < filas; i++) modelo.removeRow(0);
            } catch (Exception e) {
                System.out.println(e);
            }
            
            for (Libro llb : libros) {
                //Datos
                String serie = llb.getSerie();
                String isbn = llb.getIsbn();
                String titulo = llb.getTitulo();
                String pag = String.valueOf(llb.getPaginas());
                String precio = String.valueOf(llb.getPrecio());
                String anopub = String.valueOf(llb.getAno());
                String cantidad = String.valueOf(llb.getCantidad());
                String estado = String.valueOf(llb.getEstado().getDescripcion());
                String editorial = String.valueOf(llb.getEditorial().getNombre());
                
                String[] datos = {serie,isbn,titulo,pag,precio,anopub,cantidad,estado,editorial};
                
                modelo.addRow(datos);
            }
        } catch (Exception e) {
            System.out.println("La lista sigue nula o vacía. "+e);
        }
        
    }
    
    /**
     * Método que valida el formulario.
     * Básicamente revisa que cada campo esté completo o con datos.
     * Entrega verdadero (True) si hay campos vacíos.
     * Entrega falso (False) si no hay campos vacíos.
     * @return validación - boolean con un true o un false.
     */
    private boolean validaFormulario(){
        boolean folioVacio = vistaAgregarCompra.txtFolioFactura.getText().isEmpty();
        boolean fechaVacia = vistaAgregarCompra.jdFechaFactura.getDate() == null;
        boolean distribuidorVacio = vistaAgregarCompra.cmbDistribuidor.getSelectedIndex() < 1;
        boolean metodopagoVacio = vistaAgregarCompra.cmbMetodoPago.getSelectedIndex() < 1;
        boolean precionetoVacio = vistaAgregarCompra.txtPrecioNetoFactura.getText().isEmpty();
        boolean noseleccionalibroVacio = vistaAgregarCompra.tablaListaLibros.getSelectedRow() < 0;
        
        boolean validacion = folioVacio||fechaVacia||distribuidorVacio||metodopagoVacio||precionetoVacio||noseleccionalibroVacio;
        
        return validacion;
    }
    
}
