
package modelo;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */
public class EstadoModelo {
    //Atributos de la clase.
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    /**
     * Constructor de la clase que se encarga de generar una conexión a la base de datos.
     */
    public EstadoModelo() {
        this.entityManagerFactory = Persistence.createEntityManagerFactory("BibliotecaInformaticaPU1");
        this.entityManager = this.entityManagerFactory.createEntityManager();
    }
    
    /**
     * Método que registra un estado en la base de datos.
     * @param estado - Objeto del tipo estado.
     */
    public void agregarEstado(Estado estado){
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(estado);
        this.entityManager.getTransaction().commit();
        
    }
    
    /**
     * Método que se encarga de editar un Estado en la Base de datos.
     * @param estado - Objeto del tipo estado.
     */
    public void editarEstado(Estado estado){
        Estado estadoEdita = this.entityManager.find(Estado.class,estado.getCod_estado());
        estadoEdita.setDescripcion(estado.getDescripcion());
        
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(estadoEdita);
        this.entityManager.getTransaction().commit();
        
    }
    /**
     * Método que elimina un estado de libros registrado en la base de datos mediante su ID.
     * @param id - Int con la id del estado a eliminar.
     */
    public void eliminarEstado(int id){
        Estado estadoElimina = this.entityManager.find(Estado.class, id);
        
        this.entityManager.getTransaction().begin();
        this.entityManager.remove(estadoElimina);
        this.entityManager.getTransaction().commit();
    }
    /**
     * Función que devuelve una lista con todos los estados registrados en la Base de datos.
     * @return estados - List con objetos del tipo Estado.
     */
    public List<Estado> listaEstados(){
        
        Query query = entityManager.createQuery("SELECT e FROM Estado e");
        List<Estado> estados = query.getResultList( );
        
        return estados;
    }
    /**
     * Función que busca un estado registrado en la base de datos mediante su ID.
     * @param id - Int con la id del estado a buscar.
     * @return estado - objeto del tipo estado.
     */
    public Estado buscarEstadoID(int id){
        
        Estado estado = entityManager.find(Estado.class, id);
        
        return estado;
    }
    
}
