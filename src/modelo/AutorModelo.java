
package modelo;
//importaciones.
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 */
public class AutorModelo {
    //Atributos de la clase.
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    
    /**
     * Constructor de la clase autormodelo y que inicia la conexión a la base de datos.
     */
    public AutorModelo() {
        this.entityManagerFactory = Persistence.createEntityManagerFactory("BibliotecaInformaticaPU1");
        this.entityManager = this.entityManagerFactory.createEntityManager();
    }
    
    /**
     * Metodo para obtener la lista de autores desde la base de datos.
     * @return autores - List del tipo objeto autor.
     */
    public List<Autor> listaAutores(){
        
        Query query = entityManager.createQuery("SELECT a FROM Autor a");
        List<Autor> autores = query.getResultList( );
        
        return autores;
    }
    
    /**
     * Metodo que registra un autor en la base de datos.
     * @param autor - objeto del tipo autor.
     */
    public void crearAutor(Autor autor){
        
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(autor);
        this.entityManager.getTransaction().commit();
        
    }
    
    /**
     * Método que edita un autor en la base de datos.
     * @param autorEd - Objeto del tipo autor.
     */
    public void editaAutor(Autor autorEd){
        Autor autorEdita = this.entityManager.find(Autor.class,autorEd.getCod_autor());
        autorEdita.setNombre(autorEd.getNombre());
        autorEdita.setApe_paterno(autorEd.getApe_paterno());
        autorEdita.setApe_materno(autorEd.getApe_materno());
        
        
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(autorEdita);
        this.entityManager.getTransaction().commit();
        
    }
    
    /**
     * Método que elimina un autor en la base de datos.
     * @param id - Int con la ID del autor que se va a eliminar.
     */
    public void eliminaAutorID(int id){
        // Encontrar la venta
        Autor aut = this.entityManager.find(Autor.class, id);
        
        // Eliminar
        this.entityManager.getTransaction().begin();
        this.entityManager.remove(aut);
        this.entityManager.getTransaction().commit();
    }
    
    /**
     * Método que busca en la base de datos mediante la id del autor.
     * @param id - Int de la ID del objeto que se va a buscar.
     * @return autor - Objeto del tipo autor.
     */
    public Autor buscarAutor(int id){
        
        Autor autor = this.entityManager.find(Autor.class,id);
        
        return autor;
    }
    
    
}
