
package modelo;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

@Entity
@Table(name="distribuidor")
public class Distribuidor implements Serializable {
    //Atributos de la clase.
    @Id
    int id_dis;
    
    String rut_dis;
    String nombre;
    String calle;
    int numero;
    String comuna;
    String pais;
    String fono;
    int ano_contrato;

    public Distribuidor() {
    }

    public Distribuidor(String rut_dis, String nombre, String calle, int numero, String comuna, String pais, String fono, int ano_contrato) {
        this.rut_dis = rut_dis;
        this.nombre = nombre;
        this.calle = calle;
        this.numero = numero;
        this.comuna = comuna;
        this.pais = pais;
        this.fono = fono;
        this.ano_contrato = ano_contrato;
    }

    public String getRut_dis() {
        return rut_dis;
    }

    public void setRut_dis(String rut_dis) {
        this.rut_dis = rut_dis;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getComuna() {
        return comuna;
    }

    public void setComuna(String comuna) {
        this.comuna = comuna;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getFono() {
        return fono;
    }

    public void setFono(String fono) {
        this.fono = fono;
    }

    public int getAno_contrato() {
        return ano_contrato;
    }

    public void setAno_contrato(int ano_contrato) {
        this.ano_contrato = ano_contrato;
    }

    public int getId_dis() {
        return id_dis;
    }

    public void setId_dis(int id_dis) {
        this.id_dis = id_dis;
    }
    
    @Override
    public String toString() {
        return "Distribuidor{" + "rut_dis=" + rut_dis + ", nombre=" + nombre + ", calle=" + calle + ", numero=" + numero + ", comuna=" + comuna + ", pais=" + pais + ", fono=" + fono + ", ano_contrato=" + ano_contrato + '}';
    }
    
    
    
    
}
