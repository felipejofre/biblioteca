
package modelo;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */
public class FacturaModelo {
    //Atributos de la clase.
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    
    /**
     * Constructor de la clase que genera una conexión a la base de datos.
     */
    public FacturaModelo() {
        // Establecemos conexion con la base de datos
        this.entityManagerFactory = Persistence.createEntityManagerFactory("BibliotecaInformaticaPU1");
        this.entityManager = this.entityManagerFactory.createEntityManager();
    }
    
    /**
     * Función que registra una factura en la base de datos y devuelve el objeto registrado.
     * @param factura - Objeto del tipo factura sin la ID.
     * @return factura - objeto del tipo factura registrado.
     */
    public Factura registrarFactura(Factura factura){
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(factura);
        this.entityManager.getTransaction().commit();
        
        return factura;
    }
    
    /**
     * Método que edita una factura registrada en la base de datos.
     * @param factura - Objeto del tipo factura.
     */
    public void editarFactura(Factura factura){
        Factura facturaEdita = this.entityManager.find(Factura.class,factura.getId());
        
        facturaEdita.setFolio_fac(factura.getFolio_fac());
        facturaEdita.setPrecio_neto(factura.getPrecio_neto());
        facturaEdita.setCosto_iva(factura.getCosto_iva());
        facturaEdita.setPrecio_total(factura.getPrecio_total());
        facturaEdita.setFecha_compra(factura.getFecha_compra());
        facturaEdita.setCod_metodo(factura.getCod_metodo());
        facturaEdita.setDistribuidor(factura.getDistribuidor());
        
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(facturaEdita);
        this.entityManager.getTransaction().commit();
    }
    
    /**
     * Método que elimina una factura registrada en la base de datos mediante una id.
     * @param id - Int con la id de la factura a eliminar.
     */
    public void eliminarFactura(int id){
        Factura factura = this.entityManager.find(Factura.class, id);
        
        this.entityManager.getTransaction().begin();
        this.entityManager.remove(factura);
        this.entityManager.getTransaction().commit();
    }
    
    /**
     * Función que retorna una lista de todas las facturas registradas en la base de datos.
     * @return facturas - List con objetos del tipo factura.
     */
    public List<Factura> getListaFacturas(){
        
        Query query = entityManager.createQuery("SELECT com FROM Factura com");
        List<Factura> facturas = query.getResultList( );
        
        return facturas;
    }
    
    /**
     * Función que busca una factura registrada en la base de datos mediante su ID.
     * @param id - Int con la id de la factura a buscar.
     * @return factura - Objeto del tipo factura.
     */
    public Factura buscarFacturaID(int id){
        
        Factura factura = this.entityManager.find(Factura.class,id);
        
        return factura;
    }
    
}
