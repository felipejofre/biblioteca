
package modelo;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */
public class IdiomaModelo {
    //Atributos de la clase.
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    
    public IdiomaModelo() {
        this.entityManagerFactory = Persistence.createEntityManagerFactory("BibliotecaInformaticaPU1");
        this.entityManager = this.entityManagerFactory.createEntityManager();
    }
    
    public List<Idioma> listaIdiomas(){
        
        Query query = entityManager.createQuery("SELECT i FROM Idioma i");
        List<Idioma> idiomas = query.getResultList();
        
        return idiomas;
    }
    
    public void crearIdioma(Idioma idioma){
        
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(idioma);
        this.entityManager.getTransaction().commit();
        
    }
    
    public void editaIdioma(Idioma idiomaEdita){
        Idioma idiomaaEditar = this.entityManager.find(Idioma.class,idiomaEdita.getCod_idioma());
        idiomaaEditar.setIdioma(idiomaEdita.getIdioma());
        
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(idiomaaEditar);
        this.entityManager.getTransaction().commit();
        
        
    }
    
    public void eliminaIdiomaID(int id){
        // Encontrar la venta
        Idioma idio = this.entityManager.find(Idioma.class, id);
        
        // Eliminar
        this.entityManager.getTransaction().begin();
        this.entityManager.remove(idio);
        this.entityManager.getTransaction().commit();
    }
    
    public Idioma buscarIdiomaID(int id){
        
        Idioma idioma = entityManager.find(Idioma.class, id);
        
        return idioma;
    }
}
