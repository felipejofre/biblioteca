
package modelo;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */
public class CompraModelo {
    
    //Atributos de la clase.
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    
    /**
     * Constructor de la clase que gnera la conexión con la base de datos.
     */
    public CompraModelo() {
        // Establecemos conexion con la base de datos
        this.entityManagerFactory = Persistence.createEntityManagerFactory("BibliotecaInformaticaPU1");
        this.entityManager = this.entityManagerFactory.createEntityManager();
    }
    
    /**
     * Método que registra una compra en la base de datos.
     * @param compra - Objeto del tipo compra.
     */
    public void registrarCompra(Compra compra){
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(compra);
        this.entityManager.getTransaction().commit();
    }
    
    /**
     * Método que edita una compra registrada en la base de datos.
     * @param compra - Objeto del tipo compra.
     */
    public void editarCompra(Compra compra){
        Compra compraEdita = this.entityManager.find(Compra.class,compra.getId());
        compraEdita.setPrecio(compra.getPrecio());
        compraEdita.setFactura(compra.getFactura());
        compraEdita.setLibrosSet(compra.getLibrosSet());
        
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(compraEdita);
        this.entityManager.getTransaction().commit();
    }
    
    /**
     * Método que elimina una compra registrada en la base de datos.
     * @param id - Int con la Id de la compra a eliminar.
     */
    public void eliminarCompra(int id){
        Compra compra = this.entityManager.find(Compra.class, id);
        
        this.entityManager.getTransaction().begin();
        this.entityManager.remove(compra);
        this.entityManager.getTransaction().commit();
    }
    
    /**
     * Función que entrega una lita de todas las compras en la base de datos.
     * @return compras - List con objetos del tipo compra.
     */
    public List<Compra> getListaCompras(){
        
        Query query = entityManager.createQuery("SELECT com FROM Compra com");
        List<Compra> compras = query.getResultList( );
        
        return compras;
    }
    
    /**
     * Función que busca una compra registrada en la base de datos mediante su id.
     * @param id - Int con id de la compra a buscar.
     * @return compra - Objeto del tipo compra encontrada en la base de datos.
     */
    public Compra buscarCompraID(int id){
        
        Compra compra = this.entityManager.find(Compra.class,id);
        
        return compra;
    }
    
}
