package modelo;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */
public class LibroModelo {
    
    //Atributos de la clase.
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    
    /**
     * Constructor de la clase Libro Modelo.
     * Al crear la instancia de esta clase se inicializan las clases que manejan
     * JPA. Permite la conexión con la base de datos mediante Entity Manager Factory
     * y su unidad de persistencia. Además de y el manejo de sus acciones
     * (Editar, eliminar, buscar) con Entity Manager.
     *
     */
    public LibroModelo() {
        //Establecemos conexion con la base de datos
        this.entityManagerFactory = Persistence.createEntityManagerFactory("BibliotecaInformaticaPU1");
        this.entityManager = this.entityManagerFactory.createEntityManager();
    }
    
    /**
     * Función que agrega un libro a la DB.
     * Recibe como argumento o parámetro un objeto Libro.
     * Y luego meidante persistencia de JPA lo registra en la base de datos.
     * @param libro - Objeto del tipo Libro.
     */
    public void crearLibro(Libro libro){
        
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(libro);
        this.entityManager.getTransaction().commit();
        
    }
    
    /**
     * Método que edita un libro.
     * Recibe como parámetro un objeto libro. Busca en la DB con la ID y luego
     * mediante persistencia JPA se hace un persist para editar los nuevos valores.
     * @param libro objeto del tipo Libro.
     */
    public void editaLibro(Libro libro){
        
        Libro libroEdita = this.entityManager.find(Libro.class,libro.getLibro_id());
        
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(libroEdita);
        this.entityManager.getTransaction().commit();
        
    }
    
    /**
     * Elimina un objeto libro de la base de datos mediante su ID.
     * @param idLibro Int de la id del objeto a eliminar.
     */
    public void eliminaLibroID(int idLibro){
        // Encontrar la venta
        Libro Libro = this.entityManager.find(Libro.class, idLibro);
        
        // Eliminar
        this.entityManager.getTransaction().begin();
        this.entityManager.remove(Libro);
        this.entityManager.getTransaction().commit();
    }
    
    /**
     * Función que retorna la lista de libros.
     * Función que regorna una lista con objetos libros.
     * @return List del tipo Libro.
     */
    public List<Libro> getListaLibros(){
        Query query = entityManager.createQuery("SELECT c FROM Libro c");
        List<Libro> libros = query.getResultList();
        
        return libros;
    }
    
    /**
     * Este método genera una serie para agregar un libro.
     * Consulta a la BD. el total de registros suma y obtiene el total al
     * que le suma un entero adicional y lo formatea con 10 dígitos
     * completando con 0 los espacios faltantes.
     * @return String con la serie del nuevo libro que se quiere agregar.
     */
    public String generarSerie(){
        String noHaySerie = "0000000001";
        String serieNueva = null;
        int serieNuevaEntero = 0;
        int comparaSerie = 0;
        
        try {
            
            Query query = entityManager.createQuery("SELECT c FROM Libro c");
            
            List<Libro> libros = query.getResultList();
            
            for (Libro libro : libros) {
                serieNueva = libro.getSerie();
                serieNueva = serieNueva.substring(9);
                comparaSerie = Integer.parseInt(serieNueva);
                if (serieNuevaEntero<comparaSerie) {
                    serieNuevaEntero = comparaSerie;
                }
            }
            
            serieNuevaEntero = serieNuevaEntero +1;
            
            
            if (libros.isEmpty()) {
                serieNueva = noHaySerie;
            }else{
                serieNueva = String.format("%010d", serieNuevaEntero);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        
        
        return serieNueva;
    }
    
    /**
     * Función que se encarga de buscar un libro registrado en la base de datos mediante su ID.
     * @param id - Int con la id del libro a buscar.
     * @return libro - objeto del tipo libro.
     */
    public Libro buscarLibroTabla(int id){
        
        Libro libro = this.entityManager.find(Libro.class,id);
        
        return libro;
    }
}
