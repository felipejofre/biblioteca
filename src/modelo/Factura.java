
package modelo;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */
@Entity
@Table(name="factura")
public class Factura {
    //Atributo de la clase.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    
    private int folio_fac;
    private double precio_neto;
    private double costo_iva;
    private double precio_total;
    
    //Anotación que le indica a JPA que se trata de un atributo de tiempo.
    @Temporal(TemporalType.TIMESTAMP)
    Date fecha_compra;
    
    //Relación muchos a uno mediante JPA.
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cod_metodo")
    private MetodoPago cod_metodo;
    
    //Relación muchos a uno mediante JPA.
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_dis")
    private Distribuidor distribuidor;
    
    public Factura() {
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getFolio_fac() {
        return folio_fac;
    }
    
    public void setFolio_fac(int folio_fac) {
        this.folio_fac = folio_fac;
    }
    
    public double getPrecio_neto() {
        return precio_neto;
    }
    
    public void setPrecio_neto(double precio_neto) {
        this.precio_neto = precio_neto;
    }
    
    public double getCosto_iva() {
        return costo_iva;
    }
    
    public void setCosto_iva(double costo_iva) {
        this.costo_iva = costo_iva;
    }
    
    public double getPrecio_total() {
        return precio_total;
    }
    
    public void setPrecio_total(double precio_total) {
        this.precio_total = precio_total;
    }
    
    public Date getFecha_compra() {
        return fecha_compra;
    }
    
    public void setFecha_compra(Date fecha_compra) {
        this.fecha_compra = fecha_compra;
    }
    
    public MetodoPago getCod_metodo() {
        return cod_metodo;
    }
    
    public void setCod_metodo(MetodoPago cod_metodo) {
        this.cod_metodo = cod_metodo;
    }
    
    public Distribuidor getDistribuidor() {
        return distribuidor;
    }
    
    public void setDistribuidor(Distribuidor distribuidor) {
        this.distribuidor = distribuidor;
    }
    
//    public Compra getCompra() {
//        return compra;
//    }
//    
//    public void setCompra(Compra compra) {
//        this.compra = compra;
//    }
    
//    @Override
//    public String toString() {
//        return "Factura{" + "factura_id=" + id + ", folio_fac=" + folio_fac + ", precio_neto=" + precio_neto + ", costo_iva=" + costo_iva + ", precio_total=" + precio_total + ", fecha_compra=" + fecha_compra + ", metodoPago=" + cod_metodo + ", distribuidor=" + distribuidor + ", compra=" + compra + '}';
//    }
    
    
    
}
