
package modelo;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */
public class EditorialModelo {
    //Atributos de la clase.
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    /**
     * Constructor de la clase que genera una conexión a la base de datos.
     */
    public EditorialModelo() {
        // Establecemos conexion con la base de datos
        this.entityManagerFactory = Persistence.createEntityManagerFactory("BibliotecaInformaticaPU1");
        this.entityManager = this.entityManagerFactory.createEntityManager();
    }
    
    /**
     * Función que retorna la lista de todas las editoriales registradas en la base de datos.
     * @return editoriales - List con objetos del tipo editorial.
     */
    public List<Editorial> listaEditoriales(){
        
        Query query = entityManager.createQuery("SELECT ed FROM Editorial ed");
        List<Editorial> editoriales = query.getResultList( );
        
        return editoriales;
    }
    
    /**
     * Función que busca una editorial registrada en la base de datos mediante su ID.
     * @param id - Int del objeto editorial a buscar.
     * @return editorial - Objeto del tipo editorial.
     */
    public Editorial buscarEditorialID(int id){
        
        Editorial editorial = this.entityManager.find(Editorial.class,id);
        
        return editorial;
    }
    
    /**
     * Método que registra una editorial en la base de datos.
     * @param editorial - Objeto del tipo editorial.
     */
    public void crearEditorial(Editorial editorial){
        
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(editorial);
        this.entityManager.getTransaction().commit();
        
    }
    /**
     * Método que edita una editorial registrada en la base de datos.
     * @param editorial - Objeto del tipo editorial.
     */
    public void editarEditorial(Editorial editorial){
        
        Editorial editorialEdita = this.entityManager.find(Editorial.class,editorial.getCod_edi());
        editorialEdita.setNombre(editorial.getNombre());
        
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(editorialEdita);
        this.entityManager.getTransaction().commit();
        
    }
    /**
     * Método que elimina una editorial registrada en la base de datos mediante su ID.
     * @param id - Int con la id de la editorial a eliminar.
     */
    public void eliminarEditorialID(int id){
        
        Editorial editorial = this.entityManager.find(Editorial.class, id);
        
        this.entityManager.getTransaction().begin();
        this.entityManager.remove(editorial);
        this.entityManager.getTransaction().commit();
        
    }
}
