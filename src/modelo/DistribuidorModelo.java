
package modelo;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */
public class DistribuidorModelo {
    
    //Atributos de la clase.
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    
    /**
     * Constructor de la clase que genera una conxión a la base de datos.
     */
    public DistribuidorModelo() {
        // Establecemos conexion con la base de datos
        this.entityManagerFactory = Persistence.createEntityManagerFactory("BibliotecaInformaticaPU1");
        this.entityManager = this.entityManagerFactory.createEntityManager();
    }
    
    /**
     * Método que registra un distribuidor en la base de datos.
     * @param distribuidor - Objeto del tipo distribuidor.
     */
    public void crearDistribuidor(Distribuidor distribuidor){
        
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(distribuidor);
        this.entityManager.getTransaction().commit();

    }
    
    /**
     * Método que edita un distribuidor registrado en la base de datos.
     * @param distribuidor - Objeto del tipo distribuidor.
     */
    public void editaDistribuidor(Distribuidor distribuidor){
        
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(distribuidor);
        this.entityManager.getTransaction().commit();
        
    }
    
    /**
     * Método que elimina un registro de Distribuidor mediante su ID.
     * @param id - Int con la id del distribuidor a borrar.
     */
    public void eliminaDistribuidorID(int id){
        Distribuidor distribuidorElimina = this.entityManager.find(Distribuidor.class, id);
        
        this.entityManager.getTransaction().begin();
        this.entityManager.remove(distribuidorElimina);
        this.entityManager.getTransaction().commit();
    }
    
    /**
     * Función que devuelve una lista de objetos distribuidor.
     * @return distribuidores - List con objetos distribuidor.
     */
    public List<Distribuidor> listaDistribuidores(){
        
        Query query = entityManager.createQuery("SELECT o FROM Distribuidor o");
        List<Distribuidor> distribuidores = query.getResultList();
        
        return distribuidores;
    }

    /**
     * Función que busca un distribuidor registrado en la Base de datos mediante su ID.
     * @param id - Id del distribuidor a buscar.
     * @return distribuidor - Objeto del tipo distribuidor.
     */
    public Distribuidor buscarDistribuidor(int id){
        
        Distribuidor distribuidor = this.entityManager.find(Distribuidor.class,id);
        
        return distribuidor;
    }
    
}
