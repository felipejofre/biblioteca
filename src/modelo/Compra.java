
package modelo;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

@Entity
@Table(name="compra")
public class Compra {
    //Atributos de la clase.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    double precio;
    
    //Relación muchos a muchos mediante JPA.
    @ManyToMany
    @JoinTable(name = "libro_compra",
            joinColumns = {
                @JoinColumn(name = "compra_id")
            },
            inverseJoinColumns = {
                @JoinColumn(name = "libro_id")
            }
    )
    private Set<Libro> librosSet;
    
    //Relación muchos a uno mediante JPA.
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="factura_id")
    private Factura factura;
    
    public Compra() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public Set<Libro> getLibrosSet() {
        return librosSet;
    }

    public void setLibrosSet(Set<Libro> librosSet) {
        this.librosSet = librosSet;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    @Override
    public String toString() {
        return "Compra{" + "id=" + id + ", precio=" + precio + ", librosSet=" + librosSet + ", factura=" + factura + '}';
    }

    
    
}
