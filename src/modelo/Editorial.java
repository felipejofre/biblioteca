
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

@Entity
@Table(name="editorial")
public class Editorial implements Serializable {
    //Atributos de la clase.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cod_edi")
    int cod_edi;
    
    String nombre;
    
    //Relación uno a muchos mediante JPA.
    @OneToMany(mappedBy = "editorial")
    private List<Libro> libros;
    
    public Editorial() {
    }
    
    public Editorial(int cod_edi, String nombre) {
        this.cod_edi = cod_edi;
        this.nombre = nombre;
    }
    
    public int getCod_edi() {
        return cod_edi;
    }
    
    public void setCod_edi(int cod_edi) {
        this.cod_edi = cod_edi;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public List<Libro> getLibros() {
        return libros;
    }
    
    public void setLibros(List<Libro> libros) {
        this.libros = libros;
    }
    
    @Override
    public String toString() {
        return "Editorial{" + "cod_edi=" + cod_edi + ", nombre=" + nombre + '}';
    }
    
    
    
}
