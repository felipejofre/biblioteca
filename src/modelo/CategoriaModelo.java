
package modelo;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */
public class CategoriaModelo {
    
    //Atributos de la clase.
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    
    /**
     * Constructor de la clase que genera la conexión con la base de datos.
     */
    public CategoriaModelo() {
        // Establecemos conexion con la base de datos
        this.entityManagerFactory = Persistence.createEntityManagerFactory("BibliotecaInformaticaPU1");
        this.entityManager = this.entityManagerFactory.createEntityManager();
    }
    
    /**
     * Método que registra una categoría en la base de datos.
     * @param cate - Objeto del tipo categoría.
     */
    public void crearCategoria(Categoria cate){
        
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(cate);
        this.entityManager.getTransaction().commit();

    }
    /**
     * Método que edita un registro de categoría en la base de datos.
     * @param catEdita - Objeto del tipo categoría.
     */
    public void editaCategoria(Categoria catEdita){
        Categoria categoriaEdita = this.entityManager.find(Categoria.class,catEdita.getCod_cate());
        categoriaEdita.setNombre(catEdita.getNombre());
        
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(categoriaEdita);
        this.entityManager.getTransaction().commit();
        
    }
    
    /**
     * Método que elimina un registro de categoria de la base de datos.
     * @param id - Int con el valor de la ID de la categoría a eliminar.
     */
    public void eliminaCategoriaID(int id){
        Categoria cat = this.entityManager.find(Categoria.class, id);
        
        this.entityManager.getTransaction().begin();
        this.entityManager.remove(cat);
        this.entityManager.getTransaction().commit();
    }
    
    /**
     * Función que retorna una lista con objetos del tipo categoria.s
     * @return categorias -  List con todas las categorias registradas en la base de datos.
     */
    public List<Categoria> listaCategorias(){
        
        Query query = entityManager.createQuery("SELECT o FROM Categoria o");
        List<Categoria> categorias = query.getResultList();
        
        return categorias;
    }

    /**
     * Función que busca una categoria mediante la id en la base de datos.
     * @param id - int con la id de la categoria a buscar.
     * @return categoria - Objeto categoria asociada a la id entregada.
     */
    public Categoria buscarCategoria(int id){
        
        Categoria categoria = this.entityManager.find(Categoria.class,id);
        
        return categoria;
    }
    
    
    
}
