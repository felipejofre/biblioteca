
package modelo;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */
public class MetodoPagoModelo {
    //Atributos de la clase.
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    
    /**
     * Constructor de la clase y que genera una conexión con la base de datos.
     */
    public MetodoPagoModelo() {
        // Establecemos conexion con la base de datos
        this.entityManagerFactory = Persistence.createEntityManagerFactory("BibliotecaInformaticaPU1");
        this.entityManager = this.entityManagerFactory.createEntityManager();
    }
    
    //Método que registra o crea un método de pago en la base de datos.
    public void crearMetodoDePago(MetodoPago mediopago){
        
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(mediopago);
        this.entityManager.getTransaction().commit();

    }
    
    /**
     * Método que edita un método de pago registrado en la base de datos.
     * @param metodoPagoEdita - Objeto del tipo MetodoPago.
     */
    public void editaMeotodoPago(MetodoPago metodoPagoEdita){
        MetodoPago metodoEditando = this.entityManager.find(MetodoPago.class,metodoPagoEdita.getCod_metodo());
        metodoEditando.setDescripcion(metodoPagoEdita.getDescripcion());
        
        this.entityManager.getTransaction().begin();
        this.entityManager.persist(metodoEditando);
        this.entityManager.getTransaction().commit();
        
    }
    
    /**
     * Método que elimina un método de pago registrado en al base de datos mediante su ID.
     * @param id - Int con la id del método de pago a borrar.
     */
    public void eliminaMetodoPagoID(int id){
        MetodoPago metodopago = this.entityManager.find(MetodoPago.class, id);
        
        this.entityManager.getTransaction().begin();
        this.entityManager.remove(metodopago);
        this.entityManager.getTransaction().commit();
    }
    
    /**
     * Función que retorna una lista de métodos de pagos registrados en la base de datos.
     * @return metodosdepagos - List con objetos del tipo MetodoPago.
     */
    public List<MetodoPago> listaMetodoPagos(){
        
        Query query = entityManager.createQuery("SELECT o FROM MetodoPago o");
        List<MetodoPago> metodosdepagos = query.getResultList();
        
        return metodosdepagos;
    }

    /**
     * Función que busca un método de pago registrado en la base de datos mediante su id.
     * @param id - Int con la id del método de pago a buscar.s
     * @return metodoPago - Objeto del tipo MetodoPago encontrado.
     */
    public MetodoPago buscarMetodoPago(int id){
        
        MetodoPago metodoPago = this.entityManager.find(MetodoPago.class,id);
        
        return metodoPago;
    }
    
}
