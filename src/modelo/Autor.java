
package modelo;
//importaciones
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

@Entity
@Table(name="autor")
public class Autor {
    //Atributos de la clase.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="cod_autor")
    int cod_autor;
    
    private String nombre;
    private String ape_paterno;
    private String ape_materno;
    
    //Relación Muchos a muchos con anotaciones JPA.
    @ManyToMany
    @JoinTable(name = "libro_autor",
               joinColumns = {
                   @JoinColumn(name = "cod_autor")
               },
               inverseJoinColumns = {
                   @JoinColumn(name = "libro_id")
               }
    )
    private Set<Libro> librosSet;
    
    public Autor() {
    }
    
    public Autor(int cod_autor) {
        this.cod_autor = cod_autor;
    }
    
    public Autor(int cod_autor, String nombre, String ape_paterno, String ape_materno) {
        this.cod_autor = cod_autor;
        this.nombre = nombre;
        this.ape_paterno = ape_paterno;
        this.ape_materno = ape_materno;
    }

    public Autor(int cod_autor, String nombre, String ape_paterno, String ape_materno, Set<Libro> librosSet) {
        this.cod_autor = cod_autor;
        this.nombre = nombre;
        this.ape_paterno = ape_paterno;
        this.ape_materno = ape_materno;
        this.librosSet = librosSet;
    }
    
    public int getCod_autor() {
        return cod_autor;
    }
    
    public void setCod_autor(int cod_autor) {
        this.cod_autor = cod_autor;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String getApe_paterno() {
        return ape_paterno;
    }
    
    public void setApe_paterno(String ape_paterno) {
        this.ape_paterno = ape_paterno;
    }
    
    public String getApe_materno() {
        return ape_materno;
    }
    
    public void setApe_materno(String ape_materno) {
        this.ape_materno = ape_materno;
    }

    public Set<Libro> getLibrosSet() {
        return librosSet;
    }

    public void setLibrosSet(Set<Libro> librosSet) {
        this.librosSet = librosSet;
    }
    
    @Override
    public String toString() {
        return nombre+" "+ape_paterno+" "+ape_materno;
    }
    
    
    
    
    
}
