
package modelo;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */
@Entity
@Table(name="idioma")
public class Idioma {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="cod_idioma")
    int cod_idioma;
    
    String idioma;
    
    @ManyToMany
    @JoinTable(name = "libro_idioma",
               joinColumns = {
                   @JoinColumn(name = "cod_idioma")
               },
               inverseJoinColumns = {
                   @JoinColumn(name = "libro_id")
               }
    )
    private Set<Libro> librosSet;

    public Idioma() {
    }

    public Idioma(int cod_idioma) {
        this.cod_idioma = cod_idioma;
    }
    

    public Idioma(int cod_idioma, String idioma) {
        this.cod_idioma = cod_idioma;
        this.idioma = idioma;
    }

    public Idioma(int cod_idioma, String idioma, Set<Libro> librosSet) {
        this.cod_idioma = cod_idioma;
        this.idioma = idioma;
        this.librosSet = librosSet;
    }
    
    

    public Idioma(String idioma) {
        this.idioma = idioma;
    }

    public int getCod_idioma() {
        return cod_idioma;
    }

    public void setCod_idioma(int cod_idioma) {
        this.cod_idioma = cod_idioma;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public Set<Libro> getLibrosSet() {
        return librosSet;
    }

    public void setLibrosSet(Set<Libro> librosSet) {
        this.librosSet = librosSet;
    }
    
    

    @Override
    public String toString() {
        return "Idioma{" + "cod_idioma=" + cod_idioma + ", idioma=" + idioma + '}';
    }
    
    
    
    
}
