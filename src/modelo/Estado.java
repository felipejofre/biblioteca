
package modelo;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

@Entity
@Table(name="estado")
public class Estado {
    //Atributos de la clase.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cod_estado")
    int cod_estado;
    
    String descripcion;
    
    //Relación uno a muchos mediante JPA.
    @OneToMany(mappedBy = "estado")
    private List<Libro> libros;

    public Estado() {
    }

    public Estado(int cod_estado, String descripcion) {
        this.cod_estado = cod_estado;
        this.descripcion = descripcion;
    }

    public int getCod_estado() {
        return cod_estado;
    }

    public void setCod_estado(int cod_estado) {
        this.cod_estado = cod_estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Libro> getLibros() {
        return libros;
    }

    public void setLibros(List<Libro> libros) {
        this.libros = libros;
    }
    
    @Override
    public String toString() {
        return "Estado{" + "cod_estado=" + cod_estado + ", descripcion=" + descripcion + '}';
    }
    
    
    
}
