
package modelo;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

@Entity
@Table(name="libro")
public class Libro implements Serializable {
    //Atrubutos de la clase.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="libro_id")
    int libro_id;
    
    String serie;
    String isbn;
    String titulo;
    int paginas;
    double precio;
    int ano;
    int cantidad;

    
    /*************
     * Se establece relación muchos a muchos ocupando tabla de Unión:
     * libro_categorias.
     * Libro tiene muchas categorias.
     * Categorias tiene muchos libros.
     */
    @ManyToMany
    @JoinTable(name = "libro_categoria",
            joinColumns = {
                @JoinColumn(name = "libro_id")
            },
            inverseJoinColumns = {
                @JoinColumn(name = "cod_cate")
            }
    )
    private Set<Categoria> categoriasSet;
    
    @ManyToMany
    @JoinTable(name = "libro_idioma",
            joinColumns = {
                @JoinColumn(name = "libro_id")
            },
            inverseJoinColumns = {
                @JoinColumn(name = "cod_idioma")
            }
    )
    private Set<Idioma> idiomasSet;
    
    @ManyToMany
    @JoinTable(name = "libro_autor",
            joinColumns = {
                @JoinColumn(name = "libro_id")
            },
            inverseJoinColumns = {
                @JoinColumn(name = "cod_autor")
            }
    )
    private Set<Autor> autorSet;
    
    @ManyToMany
    @JoinTable(name = "libro_compra",
            joinColumns = {
                @JoinColumn(name = "libro_id")
            },
            inverseJoinColumns = {
                @JoinColumn(name = "compra_id")
            }
    )
    private Set<Compra> comprasSet;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cod_edi")
    private Editorial editorial;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cod_estado")
    private Estado estado;
    
    public Libro() {
    }
    
    public Libro(String serie) {
        this.serie = serie;
    }

    public Libro(int libro_id, String serie, String isbn, String titulo, int paginas, double precio, int ano, Set<Categoria> categoriasSet, Set<Idioma> idiomasSet, Set<Autor> autorSet, Editorial editorial, Estado estado) {
        this.libro_id = libro_id;
        this.serie = serie;
        this.isbn = isbn;
        this.titulo = titulo;
        this.paginas = paginas;
        this.precio = precio;
        this.ano = ano;
        this.categoriasSet = categoriasSet;
        this.idiomasSet = idiomasSet;
        this.autorSet = autorSet;
        this.editorial = editorial;
        this.estado = estado;
    }
    
    public int getLibro_id() {
        return libro_id;
    }
    
    public void setLibro_id(int libro_id) {
        this.libro_id = libro_id;
    }
    
    public String getSerie() {
        return serie;
    }
    
    public void setSerie(String serie) {
        this.serie = serie;
    }
    
    public String getIsbn() {
        return isbn;
    }
    
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
    
    public String getTitulo() {
        return titulo;
    }
    
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    
    public int getPaginas() {
        return paginas;
    }
    
    public void setPaginas(int paginas) {
        this.paginas = paginas;
    }
    
    public double getPrecio() {
        return precio;
    }
    
    public void setPrecio(double precio) {
        this.precio = precio;
    }
    
    public int getAno() {
        return ano;
    }
    
    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Editorial getEditorial() {
        return editorial;
    }

    public void setEditorial(Editorial editorial) {
        this.editorial = editorial;
    }
    
    public Estado getEstado() {
        return estado;
    }
    
    public void setEstado(Estado estado) {
        this.estado = estado;
    }
    
    public Set<Categoria> getCategoriasSet() {
        return categoriasSet;
    }
    
    public void setCategoriasSet(Set<Categoria> categoriasSet) {
        this.categoriasSet = categoriasSet;
    }
    
    public Set<Idioma> getIdiomasSet() {
        return idiomasSet;
    }
    
    public void setIdiomasSet(Set<Idioma> idiomasSet) {
        this.idiomasSet = idiomasSet;
    }
    
    public Set<Autor> getAutorSet() {
        return autorSet;
    }
    
    public void setAutorSet(Set<Autor> autorSet) {
        this.autorSet = autorSet;
    }

    @Override
    public String toString() {
        return "Libro{" + "libro_id=" + libro_id + ", serie=" + serie + ", isbn=" + isbn + ", titulo=" + titulo + ", paginas=" + paginas + ", precio=" + precio + ", ano=" + ano + ", categoriasSet=" + categoriasSet + ", idiomasSet=" + idiomasSet + ", autorSet=" + autorSet + ", editorial=" + editorial + ", estado=" + estado + '}';
    }
    
}
