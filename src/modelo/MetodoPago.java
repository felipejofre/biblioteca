
package modelo;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

@Entity
@Table(name="metodo_pago")
public class MetodoPago {
    //Atributos de la clase.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="cod_metodo")
    private int cod_metodo;
    
    private String descripcion;
    
    //Relación uno a muchos mediante JPA.
    @OneToMany(mappedBy = "cod_metodo")
    private List<Factura> facturas;

    public MetodoPago() {
    }
    
    public MetodoPago(int cod_medoto, String descripcion) {
        this.cod_metodo = cod_medoto;
        this.descripcion = descripcion;
    }

    public MetodoPago(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCod_metodo() {
        return cod_metodo;
    }

    public void setCod_metodo(int cod_metodo) {
        this.cod_metodo = cod_metodo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Factura> getFacturas() {
        return facturas;
    }

    public void setFacturas(List<Factura> facturas) {
        this.facturas = facturas;
    }
    
    

}
