
package modelo;
//importaciones.
import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author Felipe Jofré Quevedo
 * @version 08/07/2017-A.
 * 
 */

@Entity
@Table(name="categoria")
public class Categoria implements Serializable {
    
    //Atrinutos de la clase.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="cod_cate")
    int cod_cate;
    
    String nombre;
    
    //Anotación muchos a muchos mediante JPA.
    @ManyToMany
    @JoinTable(name = "libro_categoria",
               joinColumns = {
                   @JoinColumn(name = "cod_cate")
               },
               inverseJoinColumns = {
                   @JoinColumn(name = "libro_id")
               }
    )
    private Set<Libro> librosSet;
    
    public Categoria(){
        
    }

    public Categoria(int cod_cate) {
        this.cod_cate = cod_cate;
    }

    public Categoria(int cod_cate, String nombre) {
        this.cod_cate = cod_cate;
        this.nombre = nombre;
    }
    
    public Categoria(String nombre){
        this.nombre = nombre;
    }
    
    public Categoria(int cod_cate, String nombre, Set libro_id) {
        this.cod_cate = cod_cate;
        this.nombre = nombre;
        this.librosSet = libro_id;
    }
    
    public Categoria(String nombre, Set libro_id) {
        this.nombre = nombre;
        this.librosSet = libro_id;
    }
    
    public int getCod_cate() {
        return cod_cate;
    }
    
    public void setCod_cate(int cod_cate) {
        this.cod_cate = cod_cate;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public Set getLibrosSet() {
        return librosSet;
    }
    
    public void setLibrosSet(Set librosSet) {
        this.librosSet = librosSet;
    }
    
}
