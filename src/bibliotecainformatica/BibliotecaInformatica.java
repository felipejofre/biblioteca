/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package bibliotecainformatica;

import controlador.AgregarCompraControlador;
import controlador.InicioControlador;
import static java.awt.Frame.MAXIMIZED_BOTH;
import java.util.List;
import modelo.Estado;
import modelo.EstadoModelo;
import modelo.Libro;
import modelo.LibroModelo;
import vista.AgregarCompraVista;
import vista.InicioVista;

/**
 *
 * @author iFelipe
 * 
 */

public class BibliotecaInformatica {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        InicioVista index = new InicioVista();
        index.setVisible(true);
        index.setLocationRelativeTo(null);
        index.setExtendedState(MAXIMIZED_BOTH);
        InicioControlador controlInicio = new InicioControlador(index);

//        AgregarCompraVista vistaTest = new AgregarCompraVista();
//        vistaTest.setVisible(true);
//        vistaTest.setLocationRelativeTo(null);
//        
//        AgregarCompraControlador controlCompra = new AgregarCompraControlador(vistaTest);

    }
    
}
