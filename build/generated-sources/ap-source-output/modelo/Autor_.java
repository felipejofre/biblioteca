package modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Libro;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-07-11T22:00:47")
@StaticMetamodel(Autor.class)
public class Autor_ { 

    public static volatile SingularAttribute<Autor, String> ape_paterno;
    public static volatile SetAttribute<Autor, Libro> librosSet;
    public static volatile SingularAttribute<Autor, String> nombre;
    public static volatile SingularAttribute<Autor, String> ape_materno;
    public static volatile SingularAttribute<Autor, Integer> cod_autor;

}