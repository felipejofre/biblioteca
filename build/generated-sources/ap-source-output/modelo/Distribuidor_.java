package modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-07-11T22:00:48")
@StaticMetamodel(Distribuidor.class)
public class Distribuidor_ { 

    public static volatile SingularAttribute<Distribuidor, Integer> id_dis;
    public static volatile SingularAttribute<Distribuidor, Integer> numero;
    public static volatile SingularAttribute<Distribuidor, String> calle;
    public static volatile SingularAttribute<Distribuidor, String> comuna;
    public static volatile SingularAttribute<Distribuidor, String> fono;
    public static volatile SingularAttribute<Distribuidor, String> rut_dis;
    public static volatile SingularAttribute<Distribuidor, String> nombre;
    public static volatile SingularAttribute<Distribuidor, String> pais;
    public static volatile SingularAttribute<Distribuidor, Integer> ano_contrato;

}