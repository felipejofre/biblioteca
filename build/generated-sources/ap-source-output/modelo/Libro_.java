package modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Autor;
import modelo.Categoria;
import modelo.Compra;
import modelo.Editorial;
import modelo.Estado;
import modelo.Idioma;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-07-11T22:00:48")
@StaticMetamodel(Libro.class)
public class Libro_ { 

    public static volatile SingularAttribute<Libro, Editorial> editorial;
    public static volatile SetAttribute<Libro, Categoria> categoriasSet;
    public static volatile SingularAttribute<Libro, Estado> estado;
    public static volatile SingularAttribute<Libro, Integer> ano;
    public static volatile SingularAttribute<Libro, String> isbn;
    public static volatile SingularAttribute<Libro, String> titulo;
    public static volatile SetAttribute<Libro, Compra> comprasSet;
    public static volatile SingularAttribute<Libro, Integer> paginas;
    public static volatile SetAttribute<Libro, Autor> autorSet;
    public static volatile SingularAttribute<Libro, Double> precio;
    public static volatile SetAttribute<Libro, Idioma> idiomasSet;
    public static volatile SingularAttribute<Libro, String> serie;
    public static volatile SingularAttribute<Libro, Integer> libro_id;
    public static volatile SingularAttribute<Libro, Integer> cantidad;

}