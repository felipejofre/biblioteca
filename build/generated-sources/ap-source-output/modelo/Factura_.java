package modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Distribuidor;
import modelo.MetodoPago;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-07-11T22:00:48")
@StaticMetamodel(Factura.class)
public class Factura_ { 

    public static volatile SingularAttribute<Factura, Double> precio_neto;
    public static volatile SingularAttribute<Factura, Double> costo_iva;
    public static volatile SingularAttribute<Factura, Date> fecha_compra;
    public static volatile SingularAttribute<Factura, Double> precio_total;
    public static volatile SingularAttribute<Factura, Integer> id;
    public static volatile SingularAttribute<Factura, MetodoPago> cod_metodo;
    public static volatile SingularAttribute<Factura, Distribuidor> distribuidor;
    public static volatile SingularAttribute<Factura, Integer> folio_fac;

}