package modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Libro;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-07-11T22:00:48")
@StaticMetamodel(Idioma.class)
public class Idioma_ { 

    public static volatile SetAttribute<Idioma, Libro> librosSet;
    public static volatile SingularAttribute<Idioma, String> idioma;
    public static volatile SingularAttribute<Idioma, Integer> cod_idioma;

}