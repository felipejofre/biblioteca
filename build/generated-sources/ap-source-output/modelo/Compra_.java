package modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Factura;
import modelo.Libro;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-07-11T22:00:48")
@StaticMetamodel(Compra.class)
public class Compra_ { 

    public static volatile SingularAttribute<Compra, Double> precio;
    public static volatile SingularAttribute<Compra, Factura> factura;
    public static volatile SetAttribute<Compra, Libro> librosSet;
    public static volatile SingularAttribute<Compra, Integer> id;

}