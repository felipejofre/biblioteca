package modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Factura;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-07-11T22:00:48")
@StaticMetamodel(MetodoPago.class)
public class MetodoPago_ { 

    public static volatile SingularAttribute<MetodoPago, String> descripcion;
    public static volatile ListAttribute<MetodoPago, Factura> facturas;
    public static volatile SingularAttribute<MetodoPago, Integer> cod_metodo;

}